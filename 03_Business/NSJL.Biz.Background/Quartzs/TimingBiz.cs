﻿using NSJL.Biz.Background.IJobs;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.Biz.Background.Quartzs
{
    public class TimingBiz
    {
        private static readonly object locks = new object();
        private static TimingBiz manage;
        public static TimingBiz GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new TimingBiz());
            }
        }

        //调度器
        IScheduler scheduler;
        //调度器工厂
        ISchedulerFactory factory;
        public void Start()
        {
            // 1.创建scheduler的引用
            factory = new StdSchedulerFactory();
            scheduler = factory.GetScheduler();

            // 3.创建 job
            IJobDetail job = JobBuilder.Create<SimpleJob>().WithIdentity("job1", "group1").Build();
            // 4.创建 trigger
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("trigger1", "group1")
                .WithCronSchedule("0 0 * * * ? ")
                .Build();
            //4、将任务与触发器添加到调度器中
            scheduler.ScheduleJob(job, trigger);
            scheduler.Start();

        }
        public void Start2(int hour, int minute)
        {
            // 1.创建scheduler的引用
            if (factory == null)
            {
                factory = new StdSchedulerFactory();
                scheduler = factory.GetScheduler();
            }
            // 3.创建 job
            IJobDetail job = JobBuilder.Create<ServerJob>().WithIdentity("job2", "group2").Build();
            // 4.创建 trigger
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("trigger2", "group2")
                .WithCronSchedule("0 " + minute + " " + hour + " ? * MON *")
                .Build();
            //4、将任务与触发器添加到调度器中
            scheduler.ScheduleJob(job, trigger);
            scheduler.Start();
        }



    }
}
