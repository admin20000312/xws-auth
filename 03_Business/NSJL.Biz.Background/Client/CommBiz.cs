﻿using NSJL.DAL.DataAccess;
using NSJL.DAL.DataModel.Entities;
using NSJL.DomainModel.Background.System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using NSJL.DomainModel.Background.Client;
using NSJL.Framework.Utils;
using Org.BouncyCastle.Crypto.Macs;
using DJ.ZEF.ThirdParty;

namespace NSJL.Biz.Background.Client
{
    public class CommBiz
    {
        public string CabinetName = null;
        public CommBiz()
        {
            var ini = new IniFile("Config/Config.ini");
            CabinetName = ini.readKey("BoxConfig", "cabinetName");
        }
        public void GetAdminInfo()
        {
            var db=new SJLDbContext();
            db.AdminInfo.ToList();
        }
        public CommonResult Login(string username, string password)
        {
            var db=new SJLDbContext();
            var info = db.AdminInfo.Where(p => p.UserName == username && p.Id=="1").FirstOrDefault();
            if (info == null)
            {
                return new CommonResult(){result = false,message = "用户不存在"};
            }
            if (info.Password != password)
            {
                return new CommonResult() { result = false, message = "密码不正确" };
            }
            return new CommonResult(){result = true};
        }
        public CommonResult EditPassword(string username,string old, string news)
        {
            var db = new SJLDbContext();
            var info = db.AdminInfo.Where(p=>p.UserName==username && p.Id=="1").FirstOrDefault();
            if (info.Password != old)
            {
                return new CommonResult() { result = false, message = "老密码错误" };
            }

            info.Password = news;
            db.SaveChanges();
            return new CommonResult() { result = true, message = "操作成功" };
        }
        public CommonResult EditPassword2(string news)
        {
            var db = new SJLDbContext();
            var info = db.AdminInfo.Where(p => p.Id == "2").FirstOrDefault();

            info.Password = news;
            db.SaveChanges();
            return new CommonResult() { result = true, message = "操作成功" };
        }

        public bool InitBoxInfo(int count,string cabname)
        {
            var db = new SJLDbContext();
            var list = db.BoxInfo.OrderBy(p => p.BoxNum).ToList();
            db.BoxInfo.RemoveRange(list);


            for (int i = 1; i <= count; i++)
            {
                var info = new BoxInfo();
                info.Id = Guid.NewGuid().ToString("N");
                info.CreateTime = DateTime.Now;
                info.BoxNum = i;
                db.BoxInfo.Add(info);
            }
            db.SaveChanges();
            return true;
        }
        public List<OpenBoxInfo> GetOpenBoxList(string ope, DateTime? start, DateTime? end)
        {
            var db = new SJLDbContext();
            var sql = db.OpenBoxInfo.AsQueryable();
            if (!string.IsNullOrWhiteSpace(ope))
            {
                sql = sql.Where(p => p.Type == ope);
            }
            if (start != null)
            {
                sql = sql.Where(p => p.CreateTime > start);
            }
            if (end != null)
            {
                sql = sql.Where(p => p.CreateTime < end);
            }
            var temp= sql.OrderByDescending(p => p.CreateTime).ToList();
            return temp;
        }
        public List<AdminLogInfo> GetAdminLogList(DateTime? start, DateTime? end)
        {
            var db = new SJLDbContext();
            if (start == null)
            {
                return db.AdminLogInfo.OrderByDescending(p => p.CreateTime).ToList();
            }
            else
            {
                return db.AdminLogInfo.Where(p => p.CreateTime >= start && p.CreateTime <= end).OrderByDescending(p => p.CreateTime).ToList();
            }
        }
        public List<GetBoxInfo> GetBoxList()
        {
            var db = new SJLDbContext();
            var list = db.BoxInfo.OrderBy(p => p.BoxNum).ToList();
            return AutoMapper.Mapper.DynamicMap<List<GetBoxInfo>>(list);
        }
        public bool EditAdminLogInfo(string str,string adminname)
        {
            var db = new SJLDbContext();
            var info = new AdminLogInfo();
            info.UserName = adminname;
            info.CreateTime = DateTime.Now;
            info.Content = str;
            info.Id = Guid.NewGuid().ToString("N");
            info.CabName = CabinetName;
            db.AdminLogInfo.Add(info);
            db.SaveChanges();
            return true;
        }
        public CommonResult ClearBoxList(List<int> list)
        {
            var db = new SJLDbContext();
            foreach (var item in list)
            {
                var info = db.BoxInfo.Where(p => p.BoxNum == item).FirstOrDefault();
                //info.GId = null;
            }
            db.SaveChanges();
            return new CommonResult() { result = true, message = "操作成功" };
        }
        public bool AllClearBoxinfo()
        {
            var db = new SJLDbContext();
            var list = db.BoxInfo.OrderBy(p => p.BoxNum).ToList();
            foreach (var item in list)
            {
                //item.GId = null;
            }
            db.SaveChanges();
            return true;
        }
        public bool LockBoxList(List<int> list)
        {
            var db = new SJLDbContext();
            foreach (var item in list)
            {
                var info = db.BoxInfo.Where(p => p.BoxNum == item).FirstOrDefault();
                info.IsLock = true;
            }
            db.SaveChanges();
            return true;
        }
        public bool UnLockBoxList(List<int> list)
        {
            var db = new SJLDbContext();
            foreach (var item in list)
            {
                var info = db.BoxInfo.Where(p => p.BoxNum == item).FirstOrDefault();
                info.IsLock = false;
            }
            db.SaveChanges();
            return true;
        }
        public List<UserInfo> GetUserList(string name)
        {
            var db = new SJLDbContext();
            var sql = db.UserInfo.AsQueryable();
            if (!string.IsNullOrWhiteSpace(name))
            {
                sql = sql.Where(p => p.UserName.Contains(name));
            }
            var list = sql.OrderByDescending(p => p.CreateTime).ToList();
            return list;
        }
        public CommonResult EditBoxType(List<int> list, int type)
        {
            var db = new SJLDbContext();

            var ulist = db.UserInfo.Where(p => p.BoxNum != null).Select(p => p.BoxNum).ToList();
            var message = "设置成功";
            foreach (var item in list)
            {
                if (type == 0)
                {
                    if (ulist.Any(p => p == item))
                    {
                        message = "部分箱门设置失败,请先解除绑定再设置";
                    }
                    else
                    {
                        var info = db.BoxInfo.Where(p => p.BoxNum == item).FirstOrDefault();
                        info.IsSingle = type;
                    }
                }
                else
                {
                    var info = db.BoxInfo.Where(p => p.BoxNum == item).FirstOrDefault();
                    info.IsSingle = type;
                }
            }
            db.SaveChanges();
            return new CommonResult(){result = true,message = message };
        }
        public bool DelUserInfo(string id)
        {
            var db = new SJLDbContext();
            var info = db.UserInfo.Where(p => p.Id == id).FirstOrDefault();
            db.UserInfo.Remove(info);
            db.SaveChanges();
            return true;
        }
        public CommonResult EditUserInfo(string id, string username,string code,string faceId, string faceBase64,int ispulic,int? boxnum)
        {
            var db = new SJLDbContext();
            if (string.IsNullOrWhiteSpace(id))
            {
                if (!string.IsNullOrWhiteSpace(code) && db.UserInfo.Any(p => p.CardNum == code))
                {
                    return new CommonResult() { result = false, message = "卡号已被占用，请更换卡号" };
                }

                var info = new UserInfo();
                info.Id = Guid.NewGuid().ToString("N");
                info.CreateTime = DateTime.Now;
                info.UserName = username;
                info.CardNum = code;
                info.IsPublicBox = ispulic;
                info.BoxNum = boxnum;
                if (faceId != null)
                {
                    info.FaceId = faceId;
                }
                if (faceBase64 != null)
                {
                    info.HeadPic = faceBase64;
                }
                db.UserInfo.Add(info);
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(code) && db.UserInfo.Where(p=>p.Id!=id).Any(p => p.CardNum == code))
                {
                    return new CommonResult() { result = false, message = "卡号已被占用，请更换卡号" };
                }
                var user = db.UserInfo.Where(p => p.Id == id).FirstOrDefault();
                user.UserName = username;
                user.CardNum = code;
                user.IsPublicBox = ispulic;
                user.BoxNum = boxnum;
                if (faceId != null)
                {
                    user.FaceId = faceId;
                }
                if (faceBase64 != null)
                {
                    user.HeadPic = faceBase64;
                }
            }
            db.SaveChanges();
            return new CommonResult() { result = true, message = "操作成功" };
        }
        public bool IsFace(string faceId)
        {
            var db = new SJLDbContext();
            return db.UserInfo.Any(p => p.FaceId == faceId);
        }
        public CommonResult EditCabinet(string name)
        {
            var db = new SJLDbContext();
            if (db.CabinetInfo.Any(p => p.Name == name))
            {
                return new CommonResult(){result = false,message = "柜号名已存在"};
            }
            var info=new CabinetInfo();
            info.Id = Guid.NewGuid().ToString("N");
            info.Name = name;
            info.CreateTime=DateTime.Now;
            db.CabinetInfo.Add(info);
            db.SaveChanges();
            return new CommonResult(){result = true};
        }
        public List<CabinetInfo> GetCabinetList()
        {
            var db = new SJLDbContext();
            return db.CabinetInfo.OrderByDescending(p=>p.CreateTime).ToList();
        }
        public bool DelCabinet(string name)
        {
            var db = new SJLDbContext();
            var info = db.CabinetInfo.Where(p => p.Name == name).FirstOrDefault();
            db.CabinetInfo.Remove(info);
            db.SaveChanges();
            return true;
        }
        public bool IsCabinet(string name)
        {
            var db = new SJLDbContext();
            return db.CabinetInfo.Any(p => p.Name == name);
        }
        public CommonResult<UserInfo> GetUserInfoWithFaceId(string faceId)
        {
            var db = new SJLDbContext();
            var info = db.UserInfo.Where(p => p.FaceId == faceId).FirstOrDefault();
            if (info == null)
            {
                return new CommonResult<UserInfo>() { result = false, message = "用户不存在" };
            }
            return new CommonResult<UserInfo>(){result = true,data = info};
        }
       

        public BoxInfo GetBoxInfo(int boxnum)
        {
            var db=new SJLDbContext();
            return db.BoxInfo.Where(p => p.BoxNum == boxnum).FirstOrDefault();
        }
        public void Fetch(UserInfo user, int boxnum, int type)
        {
            //var db = new SJLDbContext();
            //var info = db.BoxInfo.Where(p => p.BoxNum == boxnum).FirstOrDefault();

            //var open = new OpenBoxInfo();
            //open.CreateTime = DateTime.Now;
            //open.Id = Guid.NewGuid().ToString("N");
            //open.GId = info.GId;
            //open.BoxNum = boxnum;
            //open.Type = "取";
            ////open.VerCode = info.VerCode;
            //if (type == 1)
            //{
            //    open.Mode = "人脸";
            //}
            //else if (type == 2)
            //{
            //    open.Mode = "指纹";
            //}
            //else if (type == 3)
            //{
            //    open.Mode = "密码";
            //}
            ////var role = db.RoleInfo.Where(p => p.Id == user.RoleId).FirstOrDefault();
            ////open.Name = user.Name;
            ////open.Role = role.Type;
            ////open.RoleName = role.Name;
            ////db.OpenBoxInfo.Add(open);

            ////info.VerCode = null;
            //info.GId = null;
            //info.UpdateTime = DateTime.Now;
            ////info.UserId = null;

            //db.SaveChanges();
        }
        public List<GoodsInfo> GetGoodsTypeList(string name=null, string partNum=null, string labelNum=null)
        {
            var db = new SJLDbContext();
            var sql = db.GoodsInfo.AsQueryable();
            if (!string.IsNullOrWhiteSpace(name))
            {
                sql = sql.Where(p => p.Name.Contains(name));
            }
            if (!string.IsNullOrWhiteSpace(partNum))
            {
                sql = sql.Where(p => p.PartNum.Contains(partNum));
            }
            if (!string.IsNullOrWhiteSpace(labelNum))
            {
                sql = sql.Where(p => p.LabelNum.Contains(labelNum));
            }
            return sql.OrderBy(p => p.BoxNum).ToList();
        }
        public List<GoodsInfo> GetGoodsTypeList2(string type = null)
        {
            var db = new SJLDbContext();
            var sql = db.GoodsInfo.AsQueryable();
            if (!string.IsNullOrWhiteSpace(type))
            {
                sql = sql.Where(p => p.Type== type);
            }
            return sql.OrderBy(p => p.BoxNum).ToList();
        }
        public UserInfo GetUserInfoWithCode(string code)
        {
            var db = new SJLDbContext();
            return db.UserInfo.Where(p => p.CardNum == code).FirstOrDefault();
        }
        public void UpdateUserList(List<CallBack> ulist)
        {
            if (ulist == null)
            {
                return;
            }
            var db=new SJLDbContext();
            var list = db.UserInfo.ToList();
            foreach (var item in ulist)
            {
                //var info=list.Where(p => p.CId == item.id).FirstOrDefault();
                //if (info == null)
                //{
                //    if (!string.IsNullOrWhiteSpace(item.cardId))
                //    {
                //        if (list.Any(p => p.CardId == item.cardId))
                //        {
                //            continue;
                //        }
                //    }

                //    var model=new UserInfo();
                //    model.Id = Guid.NewGuid().ToString("N");
                //    model.CreateTime=DateTime.Now;
                //    model.CId = item.id;
                //    model.UserName = item.userName;
                //    model.UserCode = item.userCode;
                //    model.UserType = item.userType;
                //    model.CellId = item.cellId;
                //    model.CardId = item.cardId;
                //    db.UserInfo.Add(model);
                //}
                //else
                //{
                //    if (!string.IsNullOrWhiteSpace(item.cardId))
                //    {
                //        if (list.Where(p => p.CId != item.id).Any(p => p.CardId == item.cardId))
                //        {
                //            continue;
                //        }
                //    }

                //    info.UserName = item.userName;
                //    info.UserCode = item.userCode;
                //    info.UserType = item.userType;
                //    info.CellId = item.cellId;
                //    info.CardId = item.cardId;
                //}
            }
            db.SaveChanges();
        }
        public void UpdateGoodsList(List<CallBack> glist)
        {
            //if (glist == null)
            //{
            //    return;
            //}
            //var db = new SJLDbContext();
            //var list = db.GoodsInfo.ToList();
            //foreach (var item in glist)
            //{
            //    var info = list.Where(p => p.CId == item.id).FirstOrDefault();
            //    if (info == null)
            //    {
            //        var model = new GoodsInfo();
            //        model.Id = Guid.NewGuid().ToString("N");
            //        model.CreateTime = DateTime.Now;
            //        model.CId = item.id;
            //        model.Name = item.name;
            //        model.Epc = item.epc;
            //        model.CellID = item.cellId.ToInt32();
            //        model.Type = null;  // "在库";
            //        db.GoodsInfo.Add(model);
            //    }
            //    else
            //    {
            //        info.Name = item.name;
            //        info.Epc = item.epc;
            //        info.CellID = item.cellId.ToInt32();
            //    }
            //}
            //db.SaveChanges();
        }
        public List<BoxInfo> GetBoxListWithUserId(string userid)
        {
            //var db=new SJLDbContext();
            //var info=db.UserInfo.Where(p => p.Id == userid).FirstOrDefault();
            ////管理员
            //if (info.UserType == 1)
            //{
            //    return db.BoxInfo.Where(p => p.IsLock != true).OrderBy(p => p.BoxNum).ToList();
            //}
            //else
            //{
            //    var list = new List<int>();
            //    if (!string.IsNullOrWhiteSpace(info.CellId))
            //    {
            //        var arr = info.CellId.Split(',');
            //        foreach (var item in arr)
            //        {
            //            list.Add(item.ToInt32());
            //        }
            //    }
            //    return db.BoxInfo.Where(p => p.IsLock != true && list.Contains(p.BoxNum)).OrderBy(p => p.BoxNum).ToList();
            //}
            return null;
        }
        public List<GetBoxInfo> GetSinglePoliceList()
        {
            var db=new SJLDbContext();
            var list=db.BoxInfo.Where(p => p.IsSingle == 1).ToList();
            var mlist=AutoMapper.Mapper.DynamicMap<List<GetBoxInfo>>(list);

            var ulist = db.UserInfo.Where(p => p.BoxNum != null).Select(p=>p.BoxNum).ToList();

            foreach (var item in ulist)
            {
                var temp=mlist.Where(p => p.BoxNum == item).FirstOrDefault();
                if (temp != null)
                {
                    mlist.Remove(temp);
                }
            }

            return mlist.OrderBy(p=>p.BoxNum).ToList();
        }
        public void ImportExcelWithGoods2(DataTable dt)
        {
            var db = new SJLDbContext();
            var list = db.GoodsInfo.ToList();

            var lllist = list.Where(p => p.LabelNum != null && p.LabelNum != "").Select(p => p.LabelNum).ToList();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var temp = (string)(dt.Rows[i][0] == DBNull.Value ? null : dt.Rows[i][0]);   //唯一编码
                var temp1 = (string)(dt.Rows[i][1] == DBNull.Value ? null : dt.Rows[i][1]);  //物品名称
                var temp2 = (string)(dt.Rows[i][2] == DBNull.Value ? null : dt.Rows[i][2]);  //物料编号
                var temp3 = (string)(dt.Rows[i][3] == DBNull.Value ? null : dt.Rows[i][3]);  //标签号
                var temp4 = (string)(dt.Rows[i][4] == DBNull.Value ? null : dt.Rows[i][4]);  //原箱门号
                //var temp5 = (string)(dt.Rows[i][5] == DBNull.Value ? null : dt.Rows[i][5]);  //实际箱门号
                //var temp6 = (string)(dt.Rows[i][6] == DBNull.Value ? null : dt.Rows[i][6]);  //状态
                var temp7 = (string)(dt.Rows[i][7] == DBNull.Value ? null : dt.Rows[i][7]);  //备注
                //var temp8 = (string)(dt.Rows[i][8] == DBNull.Value ? null : dt.Rows[i][8]);  //创建时间

                if (temp3 != null)
                {
                    if (lllist.Any(p => p == temp3))
                    {
                        continue;
                    }
                }
                if (temp1 != null)
                {
                    lllist.Add(temp3);

                    var id = string.IsNullOrWhiteSpace(temp) ? Guid.NewGuid().ToString("N") : temp.Trim();
                    var info = list.Where(p => p.Id == id).FirstOrDefault();
                    if (info != null)
                    {
                        info.Name = temp1;
                        info.PartNum = temp2;

                        info.LabelNum = temp3;

                        if (!string.IsNullOrWhiteSpace(temp4))
                        {
                            info.BoxNum = temp4.Trim().ToInt32();
                        }
                        info.Remarks = temp7;
                    }
                    else
                    {
                        var model=new GoodsInfo();
                        model.Id = id;
                        model.CreateTime=DateTime.Now;
                        model.Name = temp1;
                        model.PartNum = temp2;

                        model.LabelNum = temp3;

                        if (!string.IsNullOrWhiteSpace(temp4))
                        {
                            model.BoxNum = temp4.Trim().ToInt32();
                        }
                        model.Remarks = temp7;
                        db.GoodsInfo.Add(model);
                    }
                }
            }
            db.SaveChanges();
        }
        public void DelGoodsInfo(string id)
        {
            var db=new SJLDbContext();
            var info = db.GoodsInfo.Where(p => p.Id == id).FirstOrDefault();
            db.GoodsInfo.Remove(info);
            db.SaveChanges();
        }

        public CommonResult EditGoodsInfo(string id, string name, string PartNum, string LabelNum, int boxnum, string Remarks)
        {
            var db=new SJLDbContext();
            if (string.IsNullOrWhiteSpace(id))
            {
                if (!string.IsNullOrWhiteSpace(LabelNum) && db.GoodsInfo.Any(p => p.LabelNum == LabelNum))
                {
                    return new CommonResult() { result = false, message = "标签号已被占用，请更换标签" };
                }

                var model=new GoodsInfo();
                model.Id = Guid.NewGuid().ToString("N");
                model.CreateTime=DateTime.Now;
                model.Name = name;
                model.PartNum = PartNum;
                model.LabelNum = LabelNum;
                model.BoxNum = boxnum;
                model.Remarks = Remarks;
                db.GoodsInfo.Add(model);
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(LabelNum) && db.GoodsInfo.Where(p => p.Id != id).Any(p => p.LabelNum == LabelNum))
                {
                    return new CommonResult() { result = false, message = "标签号已被占用，请更换标签" };
                }
                var info = db.GoodsInfo.Where(p => p.Id == id).FirstOrDefault();
                info.Name = name;
                info.PartNum = PartNum;
                info.LabelNum = LabelNum;
                info.BoxNum = boxnum;
                info.Remarks = Remarks;
            }
            db.SaveChanges();
            return new CommonResult(){result = true};
        }

        public CommonResult VerZhanz(string pwd)
        {
            var db=new SJLDbContext();
            var info=db.AdminInfo.Where(p => p.Id == "2").FirstOrDefault();
            if (info.Password != pwd)
            {
                return new CommonResult(){result = false,message = "密码不正确"};
            }
            return new CommonResult(){result = true};
        }

        public List<BoxInfo> GetBoxListWithType(int type)
        {
            var db=new SJLDbContext();
            if (type == 1)
            {
                return db.BoxInfo.OrderBy(p => p.BoxNum).ToList();
            }
            else
            {
                return db.BoxInfo.Where(p=>p.IsSingle!=1).OrderBy(p => p.BoxNum).ToList();
            }
        }

        public bool IsLock(int boxnum)
        {
            var db=new SJLDbContext();
            var info = db.BoxInfo.Where(p => p.BoxNum == boxnum).FirstOrDefault();
            return info.IsLock == true;
        }

        public void Deposit(string username, int boxnum, List<RFIDHelper.RFIDInfo> list, string type)
        {
            try
            {
                string str = string.Join(",", list.Select(p => p.LabelId).ToArray());
                TextLogUtil.Info("箱门：" + boxnum + ",盘到的标签：" + str);

                var db = new SJLDbContext();
                var glist = db.GoodsInfo.ToList();
                var temp = glist.Where(p => p.ReaBoxNum == boxnum && p.Type == "在库").ToList();
                foreach (var item in temp)
                {
                    if (!list.Select(p => p.LabelId).Contains(item.LabelNum))
                    {
                        item.Type = "离库";
                        item.ReaBoxNum = null;
                        //取出物品  删除记录
                        TextLogUtil.Info("取出物品  删除记录");

                        var open = new OpenBoxInfo();
                        open.CreateTime = DateTime.Now;
                        open.Id = Guid.NewGuid().ToString("N");
                        open.BoxNum = boxnum;
                        open.Type = "取";   //---------
                        open.Mode = type;
                        open.UserName = username;
                        open.GoodsName = item.Name;
                        open.LabelNum = item.LabelNum;
                        db.OpenBoxInfo.Add(open);
                    }
                }

                foreach (var item in list)
                {
                    var info = glist.Where(p => p.LabelNum == item.LabelId).FirstOrDefault();
                    if (info == null)
                    {
                        //该标签在数据库未存在  不做处理
                        TextLogUtil.Info("该标签在数据库未存在  不做处理");
                    }
                    else
                    {
                        if (info.Type == "在库")
                        {
                            if (boxnum != info.ReaBoxNum)
                            {
                                //不存在当前格子   移动物品   上传2条物品存和取   修改goods boxnum  清除 cid   物品异常 不做处理
                                TextLogUtil.Info("不存在当前格子   移动物品   上传2条物品存和取   修改goods boxnum  清除 cid   物品异常 不做处理");

                                //info.BoxNum = item.BoxNum;

                                //var open = new OpenBoxInfo();
                                //open.CreateTime = DateTime.Now;
                                //open.Id = Guid.NewGuid().ToString("N");
                                //open.BoxNum = item.BoxNum;
                                //open.Type = "存";   //---------
                                //open.Mode = type;
                                //open.UserName = user.UserName;
                                //open.UserCode = user.UserCode;
                                //open.GoodsName = info.Name;
                                //open.Epc = item.LabelId;
                                //db.OpenBoxInfo.Add(open);

                                //rlist.Add(new GetReport()
                                //{
                                //    epc = item.LabelId,
                                //    cellID = boxnum,
                                //    status = 1
                                //});
                            }
                            else
                            {
                                TextLogUtil.Info("存在当前格子无差别 不需要处理");
                                //存在当前格子无差别 不需要处理
                            }
                        }
                        else if (info.Type == "离库" || info.Type == null || info.Type == "") //刚做初始化
                        {
                            //存入操作
                            info.Type = "在库";
                            info.ReaBoxNum = boxnum;

                            var open = new OpenBoxInfo();
                            open.CreateTime = DateTime.Now;
                            open.Id = Guid.NewGuid().ToString("N");
                            open.BoxNum = boxnum;
                            open.Type = "存";   //---------
                            open.Mode = type;
                            open.UserName = username;
                            open.GoodsName = info.Name;
                            open.LabelNum = item.LabelId;
                            db.OpenBoxInfo.Add(open);
                        }
                    }
                }
                db.SaveChanges();
            }
            catch (Exception e)
            {
                TextLogUtil.Info("写入数据库失败" + e.Message);
            }

        }
    }
}
