﻿using NSJL.DomainModel.Background.Client;
using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace NSJL.Biz.Background.Client
{
    public class InterfaceBiz
    {
        public string corpCode = null;
        public string deviceCode = null;
        public string url = null;
        public InterfaceBiz()
        {
            var ini = new IniFile("Config/Config.ini");
            corpCode = ini.readKey("InterfaceConfig", "corpCode");
            deviceCode = ini.readKey("InterfaceConfig", "deviceCode");
            url = ini.readKey("InterfaceConfig", "url");
        }
        public List<CallBack> GetUserInfo()
        {
            try
            {
                //var postdata=new GetIntClass();
                //postdata.corpCode = corpCode;
                //postdata.deviceCode = deviceCode;
                //postdata.pageNum = 1;
                //postdata.pageSize = 20000;

                var flag = 0;
                ReStart:
                var result = GetRequest(url + "/cab/user/query_list?corpCode="+ corpCode + "&deviceCode="+ deviceCode + "&pageNum=1&pageSize=20000");
                if (!result.result)
                {
                    if (flag < 3)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                var data = new JavaScriptSerializer().Deserialize<GetCallBack<List<CallBack>>>(result.data);
                if (data.success == true)
                {
                    return data.data;
                }   
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public List<CallBack> GetGoodsList()
        {
            try
            {
                var flag = 0;
                ReStart:
                var result = GetRequest(url + "/cab/thing/query_list?deviceCode=" + deviceCode + "&corpCode="+ corpCode + "&pageNum=1&pageSize=20000");
                if (!result.result)
                {
                    if (flag < 3)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                var data = new JavaScriptSerializer().Deserialize<GetCallBack<List<CallBack>>>(result.data);
                if (data.success == true)
                {
                    return data.data;
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public CommonResult RepInventory(string userCode,List<GetReport> list)
        {
            try
            {
                var ints=new GetInt2Class<List<GetReport>>();
                ints.corpCode = corpCode;
                ints.deviceCode = deviceCode;
                ints.userCode = userCode;
                ints.data = list;

                var flag = 0;
                ReStart:
                var result = GetRequest(url + "/cab/inv/upload", new JavaScriptSerializer().Serialize(ints),"POST");
                if (!result.result)
                {
                    if (flag < 3)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                var data = new JavaScriptSerializer().Deserialize<GetCallBack<List<CallBack>>>(result.data);
                if (data.success == true)
                {
                    return new CommonResult(){result = true};
                }
                return new CommonResult() { result = false,message = data.msg};
            }
            catch (Exception e)
            {
                return new CommonResult(){result = false};
            }
        }
        public DateTime? GetSystemTime()
        {
            try
            {
                var flag = 0;
                ReStart:
                var result = GetRequest(url + "/cab/sys/get_time?corpCode=" + corpCode + "&deviceCode="+ deviceCode);
                if (!result.result)
                {
                    if (flag < 3)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                var data=new JavaScriptSerializer().Deserialize<GetCallBack<SysTime>>(result.data);
                if (data.success == true)
                {
                    return data.data.systemTime.ToDateTime();
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public static CommonResult GetRequest(string url, string data=null,string method= "GET")
        {
            try
            {
                //TextLogUtil.Info("请求url："+url+"-----参数："+(data??""));

                ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => { return true; };
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                ServicePointManager.DefaultConnectionLimit = 2000;

                string strBuff = "";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = "application/json";
                request.Method = method;
                request.ServicePoint.Expect100Continue = false;
                request.Timeout = 1000 * 10;
                request.ReadWriteTimeout = 1000 * 10;

                if (!string.IsNullOrWhiteSpace(data))
                {
                    byte[] postdata = System.Text.Encoding.UTF8.GetBytes(data);
                    request.ContentLength = postdata.Length;
                    Stream newStream = request.GetRequestStream();
                    newStream.Write(postdata, 0, postdata.Length);
                    newStream.Close();
                }

                HttpWebResponse httpResp = (HttpWebResponse)request.GetResponse();
                Stream respStream = httpResp.GetResponseStream();
                StreamReader respStreamReader = new StreamReader(respStream, Encoding.UTF8);
                strBuff = respStreamReader.ReadToEnd();
                request.Abort();
                httpResp.Close();
                if (respStream != null)
                {
                    respStream.Close();
                }
                respStreamReader.Close();

                //TextLogUtil.Info("返回："+strBuff);

                return new CommonResult() { result = true, data = strBuff };
            }
            catch (Exception ex)
            {
                //TextLogUtil.Info(ex.Message);
                return new CommonResult() { result = false, message = ex.Message };
            }
        }
    }
}
