﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSJL.DAL.DataAccess;
using NSJL.DomainModel.Background.Client;
using NSJL.Framework.Utils;
using NSJL.Plugin.Excel;
using System.IO;
using NSJL.Biz.Background.Client;

namespace NSJL.Biz.Background.IJobs
{
    public class SimpleJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var ibiz = new InterfaceBiz();
                var biz = new CommBiz();
                var ulist = ibiz.GetUserInfo();
                biz.UpdateUserList(ulist);
                var glist = ibiz.GetGoodsList();
                biz.UpdateGoodsList(glist);
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }
        }
    }
}