﻿using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.Plugin.Third.ThirdParty
{
    public class SerialPortHelp
    {
        //SerialPortHelp sp = new SerialPortHelp();
        //sp.Start("COM8",19200);
        //var send = new byte[] { 0x55, 0x01, 0xa1, 0xff, 0x00 };
        //sp.SendByte(send);
        public SerialPort sp = null;
        public Action<byte[]> CallBackAction;
        public CommonResult Start(string comname, int baudRate = 9600, Parity parity = Parity.None, int dataBits = 8, StopBits stopBits = StopBits.One)
        {
            try
            {
                sp = new SerialPort(comname, baudRate, parity, dataBits, stopBits);
                //sp.RtsEnable = true;
                //sp.DtrEnable = true;
                if (!sp.IsOpen)
                {
                    sp.Open();
                }
                sp.DataReceived += new SerialDataReceivedEventHandler(DataReceived);
                return new CommonResult() { result = true };
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
                return new CommonResult() { result = false, message = comname + e.Message };
            }
            return new CommonResult() { result = false, message = comname + "打开串口失败" };
        }
        //测试箱门命令
        //var send = new byte[] { 0x55, 0x01, 0xa1, 0xff, 0x00 };
        public bool SendByte(byte[] bytes)
        {
            try
            {
                if (!sp.IsOpen)
                {
                    sp.Open();
                }
                sp.Write(bytes, 0, bytes.Length);

                return true;
            }
            catch (Exception e)
            {
                TextLogUtil.Info(sp.PortName + "：报错：" + e.Message+"--"+BitConverter.ToString(bytes));
                return false;
            }
        }
        public void DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //var sp = (SerialPort) sender;
            if (sp.BytesToRead > 0)
            {
                //sp.ReadBufferSize
                var count = sp.BytesToRead;
                var buff = new byte[count];
                sp.Read(buff, 0, count);

                //var text = BitConverter.ToString(buff);
                //TextLogUtil.Info(this.sp.PortName + "-接收到" + text);


                if (CallBackAction != null)
                {
                    CallBackAction(buff);
                }
            }
        }
        public void Close()
        {
            try
            {
                if (sp != null && sp.IsOpen)
                {
                    sp.Close();
                    sp.Dispose();
                    sp = null;
                }
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }
        }


    }
}
