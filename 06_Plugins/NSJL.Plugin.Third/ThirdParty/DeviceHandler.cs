/*===========================================================================================
 * Copyright (C) 2012 Fingerprint Device Source for ANSI/ISO Standard
 *
 * Licensed under License;
 * you may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" ,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *==========================================================================================*/
using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Collections.Generic;

//---------------------------------------------------------------------------------------------
public class DeviceHandler
{
    //-----------------------------------------------------------------------------------------
    // INTERNAL CONTEXT
    //-----------------------------------------------------------------------------------------
    [DllImport("FpStdLib.dll")]
    public static extern int FpStdLib_OpenDevice();
    [DllImport("FpStdLib.dll")]
    public static extern void FpStdLib_CloseDevice(int device);
    [DllImport("FpStdLib.dll")]
    public static extern int FpStdLib_Calibration(int device);
    [DllImport("FpStdLib.dll")]
    public static extern int FpStdLib_GetImage(int device, IntPtr image); 
    [DllImport("FpStdLib.dll")]
    public static extern int FpStdLib_GetImageQuality(int device, IntPtr image);
    [DllImport("FpStdLib.dll")]
    public static extern int FpStdLib_CreateANSITemplate(int device, IntPtr image, IntPtr itemplate);
    [DllImport("FpStdLib.dll")]
    public static extern int FpStdLib_CompareTemplates(int device, IntPtr sTemplate, byte[] fTemplate);
    [DllImport("FpStdLib.dll")]
    public static extern int FpStdLib_CompareTemplates(int device, byte[] sTemplate, byte[] fTemplate);
    [DllImport("FpStdLib.dll")]
    public static extern int FpStdLib_SearchingANSITemplates(int device, IntPtr sTemplate, int arrayCnt, byte[] fTemplateArray, int matchedScoreTh);
     //-----------------------------------------------------------------------------------------
    public const int DEF_FPIMG_QUALITY = 50;
    public const int DEF_CAPTURE_TIMEOUT = 10000;
    //-----------------------------------------------------------------------------------------
    public const int DEF_STATUS_ERROR = -1;
    public const int DEF_STATUS_CAPTURED = 0;
    public const int DEF_STATUS_ANSI_FMR = 1;
    public const int DEF_STATUS_ISO_FMR = 2;
    //-----------------------------------------------------------------------------------------
    int CUR_CAPTURE_TIMEOUT = DEF_CAPTURE_TIMEOUT;
    int CUR_FPIMG_QUALITY = DEF_FPIMG_QUALITY;
    //-----------------------------------------------------------------------------------------
    const int FLAG_CAPTURE_STARTED = 1;
    const int FLAG_CAPTURE_STOPPED = 0;
    const int IMAGE_WIDTH = 256;
    const int IMAGE_HEIGHT = 360;
    const int IMAGE_SIZE = IMAGE_WIDTH * IMAGE_HEIGHT;
    const int FMR_ISO_STD_MAX_SIZE = 1024;
    //-----------------------------------------------------------------------------------------
    int    DeviceHandle = 0;
    IntPtr RawImgMap; 
    IntPtr AnsiFmrMap;
    IntPtr AnsiFirMap;
    byte[] RawImage = null;
    int    CpatureStatus = FLAG_CAPTURE_STOPPED;
    //-----------------------------------------------------------------------------------------
    // FOR COMMON API OF UIDAI
    //-----------------------------------------------------------------------------------------
    public delegate void HandlerFunction(
            byte[] ImgData, int height, int width, int status, string ErrMsg, bool complete, byte[] FmrData);
    public event HandlerFunction CallHandler = null;
    //-----------------------------------------------------------------------------------------
    public DeviceHandler()
	{
        DeviceHandle = 0;
        CUR_CAPTURE_TIMEOUT = DEF_CAPTURE_TIMEOUT;
    }
    public int InitDevice()
    {
        DeviceHandle = FpStdLib_OpenDevice();
        if (DeviceHandle == 0) return 1;
        RawImgMap = Marshal.AllocHGlobal(IMAGE_SIZE);
        AnsiFirMap = Marshal.AllocHGlobal(FMR_ISO_STD_MAX_SIZE); 
        AnsiFmrMap = Marshal.AllocHGlobal(FMR_ISO_STD_MAX_SIZE); 
        RawImage = new byte[IMAGE_SIZE];
        CpatureStatus = FLAG_CAPTURE_STOPPED;
        return 0;
    }
    public void UnInitDevice()
    {
        if (CpatureStatus == FLAG_CAPTURE_STARTED) return;
        if (DeviceHandle == 0) return;
        FpStdLib_CloseDevice(DeviceHandle);
        Marshal.FreeHGlobal(RawImgMap);
        Marshal.FreeHGlobal(AnsiFirMap);
        //Marshal.FreeHGlobal(IsoFirMap);
        Marshal.FreeHGlobal(AnsiFmrMap);
        //Marshal.FreeHGlobal(AnsiFirMap);
    }
    public void SetThresholdQuality(int val)
    {
        CUR_FPIMG_QUALITY = val;
    }
    public void SetCaptureTimeout(int val)
    {
        CUR_CAPTURE_TIMEOUT = val;
    }
    public void BeginCapture()
    {
        if (CpatureStatus == FLAG_CAPTURE_STARTED) return;
        Thread t = new Thread(new ThreadStart((on_capturing_image)));
        t.IsBackground = true; 
        t.Start(); 
    }
    public void on_capturing_image()
    {
        if (DeviceHandle == 0)
        {
            CallHandler(null, 0, 0, DEF_STATUS_ERROR, "ERROR : Device isn't openned.", false, null);
            return;
        }
        if (CallHandler == null || CUR_CAPTURE_TIMEOUT <= 0)
        {
            CallHandler(null, 0, 0, DEF_STATUS_ERROR, "ERROR : Setting is mistaken.", false, null);
             return;
        }

        DateTime startTime = DateTime.Now;
        CpatureStatus = FLAG_CAPTURE_STARTED;
        while (true)
        {
            FpStdLib_GetImage(DeviceHandle, RawImgMap);
            Marshal.Copy(RawImgMap, RawImage, 0, IMAGE_SIZE);
            CallHandler(RawImage, IMAGE_HEIGHT, IMAGE_WIDTH, DEF_STATUS_CAPTURED, "", false, null);
            int qr = FpStdLib_GetImageQuality(DeviceHandle, RawImgMap);
            if (qr > CUR_FPIMG_QUALITY)
            {
                CallHandler(RawImage, IMAGE_HEIGHT, IMAGE_WIDTH, DEF_STATUS_CAPTURED, "", true, null);
                break;
            }
            if ((DateTime.Now - startTime) > TimeSpan.FromMilliseconds(CUR_CAPTURE_TIMEOUT))
            {
                CallHandler(null, 0, 0, DEF_STATUS_ERROR, "ERROR : Timeout.", false, null);
                break;
            }
        }
        CpatureStatus = FLAG_CAPTURE_STOPPED;
    }
    public int GetImageQuality()
    {
        return FpStdLib_GetImageQuality(DeviceHandle, RawImgMap);
    }

    public int CreateANSITemplate1()
    {
        int size = FpStdLib_CreateANSITemplate(DeviceHandle, RawImgMap, AnsiFmrMap);
        if (size >= 0)
        {
            byte[] tmpFMR = new byte[size];
            Marshal.Copy(AnsiFmrMap, tmpFMR, 0, size);
            CallHandler(null, 0, 0, DEF_STATUS_ANSI_FMR, "", true, tmpFMR);
        }
        return size;
    }
    public int CreateANSITemplate2()
    {
        int size = FpStdLib_CreateANSITemplate(DeviceHandle, RawImgMap, AnsiFirMap);
        if (size >= 0)
        {
            byte[] tmpFMR = new byte[size];
            Marshal.Copy(AnsiFirMap, tmpFMR, 0, size);
            CallHandler(null, 0, 0, DEF_STATUS_ISO_FMR, "", true, tmpFMR);
        }
        return size;
    }
    public int CompareTemplates()
    {
        return 0;
        //return FpStdLib_CompareTemplates(DeviceHandle, AnsiFmrMap, AnsiFirMap);
    }
  
    //-----------------------------------------------------------------------------------------
}
