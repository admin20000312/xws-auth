﻿using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;
using RFIDReaderNetwork_SerialSDK_ForCSharp.DataStructureLayer;
using RFIDReaderNetwork_SerialSDK_ForCSharp.ExternalInterfaceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.Plugin.Third.ThirdParty
{
    public class RFIDHelper2
    {
        private static readonly object locks = new object();
        private static RFIDHelper2 manage;
        public static RFIDHelper2 GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new RFIDHelper2());
            }
        }
        public Action<string, int> CallBack;

        private RFIDClient rfidClientReader;

        private RFIDServer rifdServer;
        private RFIDServerModel rifdServerModel;

        public OperatReader m_rfidWorkReader { get; set; }

        public void ConnectNetWork(int port = 7880)
        {

            rifdServer = new RFIDServer(port);
            var nRetVal = rifdServer.StartServer();                      //开启服务
            if (nRetVal == OperationResult.SUCCESS)
            {
                rifdServer.m_OnErrorOccured += new EventHandler<ErrorReportEventArgs>(onErrorOccuredEven);
                rifdServer.m_OnRegistered += new EventHandler<RegisteredEventArgs>(m_rifdServer_m_OnRegistered);
                rifdServer.m_OnUnregistered += new EventHandler<UnregisteredEventArgs>(m_rifdServer_m_OnUnregistered);
                rifdServer.m_OnInventoryReport += new EventHandler<InventoryReportEventArgs>(onInventoryReport);
            }
            else
            {
                TextLogUtil.Info("初始化UHF失败！错误码是 " + port + nRetVal.ToString() + "--------UHF设备错误");
                return;
            }
        }
        public CommonResult ConnectCOM(string portName, int baudRate = 115200)
        {
            if (rfidClientReader != null)
            {
                rfidClientReader.Disconnect();
            }
            rfidClientReader = new RFIDClient();
            OperationResult result = rfidClientReader.ConnectSerial(portName.ToUpper(), baudRate);
            if (result != OperationResult.SUCCESS)
            {
                TextLogUtil.Info("初始化UHF失败！错误码是 " + portName + result.ToString() + "--------UHF设备错误");

                return new CommonResult() { result = false, message = "初始化盘点器失败" };
            }
            //断开连接
            rfidClientReader.m_OnDisconnect += new EventHandler<DisconnectEventArgs>(onDisconnectEven);
            //报错的
            rfidClientReader.m_OnErrorcallback += new EventHandler<ErrorReportEventArgs>(onErrorOccuredEven);
            //盘点的时候
            rfidClientReader.m_OnInventoryReport += new EventHandler<InventoryReportEventArgs>(onInventoryReport);

            m_rfidWorkReader = rfidClientReader;

            return new CommonResult() { result = true };
        }

        //串口模式
        //盘点事件通用
        private void onInventoryReport(object sender, InventoryReportEventArgs e)
        {
            if (CallBack != null)
            {
                CallBack(e.m_stInventoryResult.m_strEPC, e.m_stInventoryResult.m_strAntennaNo.ToInt32());
            }
        }
        //错误事件通用
        private void onErrorOccuredEven(object sender, ErrorReportEventArgs e)
        {
            TextLogUtil.Info(e.m_strErrorCode.ToString() + "------UHF设备错误");
        }
        private void onDisconnectEven(object sender, DisconnectEventArgs e)
        {
            TextLogUtil.Info("UHF设备失去连接----UHF设备错误");
            //Start();
        }

        //网络 TCP 模式
        //链接成功
        private void m_rifdServer_m_OnRegistered(object sender, RegisteredEventArgs e)
        {
            if (e != null)
            {
                OperationResult nRetVal = OperationResult.FAIL;
                rifdServerModel = new RFIDServerModel(e.m_strDeviceID);

                m_rfidWorkReader = rifdServerModel;

                //同步时间
                DateTime day = DateTime.Now;
                int month = day.Month;
                int d = day.Day;
                int h = day.Hour;
                int mm = day.Minute;
                int y = day.Year;
                int s = day.Second;
                string time;
                time = String.Format("{0:D2}{1:D2}{2:D2}{3:D2}{4:D4}.{5:D2}", month, d, h, mm, y, s);
                nRetVal = rifdServerModel.SetCurrentTime(time);
                if (nRetVal == OperationResult.SUCCESS)
                {

                }
                else
                {
                    TextLogUtil.Info(nRetVal.ToString());
                }
            }
        }
        //断开链接
        private void m_rifdServer_m_OnUnregistered(object sender, UnregisteredEventArgs e)
        {
            TextLogUtil.Info("UHF设备失去连接----UHF设备错误");
        }

        public void StartRead()
        {
            OperationResult result = m_rfidWorkReader.StartPerioInventory();
            if (result != OperationResult.SUCCESS)
            {
                TextLogUtil.Info("UHF设备读取数据命令失败！错误码是" + result.ToString());
            }
        }
        public void StopRead()
        {
            OperationResult result = m_rfidWorkReader.StopPeriodInventory();
            if (result != OperationResult.SUCCESS)
            {
                TextLogUtil.Info("UHF设备停止读取数据命令失败！错误码是" + result.ToString());
            }
        }

        //GetHummer   蜂鸣器   开关  1 启用   0  禁用
        public void StartHummer(int state = 1)
        {
            Hummer hummerInfo = new Hummer();
            UInt32 data = 0xff;
            OperationResult nRetVal = OperationResult.FAIL;
            hummerInfo.State = state;
            nRetVal = m_rfidWorkReader.GetHummer(ref hummerInfo);
            if (nRetVal != OperationResult.SUCCESS)
            {
                TextLogUtil.Info("设置蜂鸣器失败：原因:" + nRetVal.ToString());
            }

            nRetVal = m_rfidWorkReader.SetLed(1, data);
            if (nRetVal != OperationResult.SUCCESS)
            {
                TextLogUtil.Info("设置蜂鸣器失败：原因:" + nRetVal.ToString());
            }
        }


        //传入 天线ID  循环盘点5次  返回  盘到的标签和天线号   天线号  是箱门号的-1
        public List<DJ.ZEF.ThirdParty.RFIDHelper.RFIDInfo> StartOnceReadByAnt(int boxnum, int Num = 15)
        {
            var antId = boxnum - 1;   //天线号  是箱门号的-1

            TextLogUtil.Info("开始盘点天线号：" + antId);
            var list = new List<DJ.ZEF.ThirdParty.RFIDHelper.RFIDInfo>();
            for (var j = 0; j < Num; j++)
            {
                var tagReport = new TagReport();
                OperationResult result = m_rfidWorkReader.Inventory(antId, ref tagReport);
                if (result == OperationResult.SUCCESS)
                {
                    for (var z = 0; z < tagReport.m_listTags.Count; z++)
                    {
                        var info = new DJ.ZEF.ThirdParty.RFIDHelper.RFIDInfo();
                        info.LabelId = tagReport.m_listTags[z].m_strEPC;
                        info.AntennaNo = tagReport.m_listTags[z].m_nAntennaNo;
                        info.BoxNum = info.AntennaNo + 1;

                        if (!list.Any(p => p.LabelId == info.LabelId))
                        {
                            TextLogUtil.Info("天线号：" + info.AntennaNo + "盘点到：" + info.LabelId);
                            list.Add(info);
                        }
                    }
                }
            }
            TextLogUtil.Info("结束盘点天线号：" + antId);
            return list;
        }
        //public class RFIDInfo
        //{
        //    //标签号
        //    public string LabelId { get; set; }
        //    //天线号
        //    public int AntennaNo { get; set; }
        //    //箱门号
        //    public int BoxNum { get; set; }
        //}

        public void Close()
        {
            if (rfidClientReader != null)
            {
                var nRetVal = rfidClientReader.Disconnect();
                if (nRetVal != OperationResult.SUCCESS)
                {
                    TextLogUtil.Info("断开读写器失败,原因可能为" + nRetVal.ToString());
                    return;
                }
                rfidClientReader = null;
            }
            if (rifdServer != null)
            {
                rifdServer.StopServer();
                rifdServerModel = null;
                rifdServer = null;
            }
            CallBack = null;
            m_rfidWorkReader = null;
        }

    }
}
