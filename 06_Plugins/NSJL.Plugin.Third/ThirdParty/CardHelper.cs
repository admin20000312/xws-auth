﻿using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;
using NSJL.Plugin.Third.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DJ.ZEF.ThirdParty
{
    public class CardHelper
    {
        private static readonly object locks = new object();
        private static CardHelper manage;
        public static CardHelper GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new CardHelper());
            }
        }
        private CachedBuffer cache = new CachedBuffer();
        public Action<string> CallBackStr = null;
        private SerialPortHelp sp = null;
        //0表示 485  刷卡    1 表示TTL刷卡器+10进制ID刷卡器    2  16进制ID刷卡器
        public int CardType { get; set; } = 0;
        public CommonResult Start(string portName, int baudRate,Action<string> action, int cardType = 0)
        {
            try
            {
                cache.Clear();

                CallBackStr = action;
                CardType = cardType;
                if (sp == null || !sp.sp.IsOpen)
                {
                    sp = new SerialPortHelp();
                    sp.CallBackAction = DataReceived;
                    return sp.Start(portName, baudRate);
                }
                return new CommonResult() { result = true };
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
                return new CommonResult(){result = false};
            }
        }
        public void DataReceived(byte[] buffer)
        {
            try
            {
                cache.Write(buffer, 0, buffer.Length);
                var temp = cache.ReadDataWithCard(CardType);
                if (temp == null)
                {
                    return;
                }

                if (CallBackStr != null)
                {
                    if (buffer.Length == 14)
                    {//TTL刷卡器
                        ////去掉4个字节 头1 尾3
                        var bytes = new byte[10];
                        Array.Copy(temp, 1, bytes, 0, 10);

                        var str = Encoding.ASCII.GetString(bytes);
                        Task.Run(() =>
                        {
                            CallBackStr(str);
                        });
                        return;
                    }

                    if (temp.Length == 12)
                    {//485刷卡器
                        var bytes = new byte[4];
                        Array.Copy(temp, 7, bytes, 0, 4);

                        var no = BitConverter.ToString(bytes).Replace("-", "");
                        Task.Run(() =>
                        {
                            CallBackStr(no);
                        });
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }
        }

        public void Stop()
        {
            try
            {
                CallBackStr = null;
                //sp.Close();
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }
        }

    }
}
