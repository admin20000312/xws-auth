﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DJ.ZEF.ThirdParty
{

    public class FaceAttributeInfoS
    {
        public FaceAttributeDataS data { get; set; }

        public int errno { get; set; }

        public string msg { get; set; }
    }
    public class FaceAttributeDataS
    {
        public string face_token { get; set; }
        public int result_num { get; set; }
        public decimal score { get; set; }
        public string log_id { get; set; }
        public FaceAttributeResult result { get; set; }
    }


    public class FaceAttributeInfo
    {
        public FaceAttributeData data { get; set; }

        public string errno { get; set; }

        public string msg { get; set; }
    }
    public class FaceAttributeData
    {
        public string face_token { get; set; }
        public int result_num { get; set; }
        public decimal score { get; set; }
        public string log_id { get; set; }
        public List<FaceGroupResult> result { get; set; }
        public List<user_id_list> user_id_list { get; set; }
    }

    public class user_id_list
    {
        public string user_id { get; set; }
    }
    public class FaceGroupResult
    {
        public string group_id { get; set; }
        public decimal score { get; set; }
        public string user_id { get; set; }
    }
    public class FaceAttributeResult
    {
        public float age { get; set; }
        /// <summary>
        /// 表情
        /// </summary>
        public float expression { get; set; }
        public float expression_conf { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public float gender { get; set; }
        public float gender_conf { get; set; }
        public float glass { get; set; }
        public float glass_conf { get; set; }
        /// <summary>
        /// 种族
        /// </summary>
        public float race { get; set; }
        public float race_conf { get; set; }
    }
}
