﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;

namespace DJ.ZEF.ThirdParty
{
//    var result = VideoHelper.GetInstance().Start(pictureBox1, (a, b) =>
//{
//File.WriteAllBytes(@"C:\Users\Administrator\Desktop\" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".jpg", a);
//MessageBox.Show(b);
//});
//if (!result.result)
//{
//MessageBox.Show(result.message);
//}
    public class VideoHelper
    {
        private static readonly object locks = new object();
        private static VideoHelper manage;
        public static VideoHelper GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new VideoHelper());
            }
        }

        public VideoCaptureDevice videoSource;
        public Action<Bitmap> BitMapCallBack;
        public Action<byte[]> CallBackAction;
        public CommonResult Start(Action<Bitmap> bitaction, Action<byte[]> action)
        {
            try
            {
                BitMapCallBack = bitaction;
                CallBackAction = action;

                if (videoSource == null)
                {
                    FilterInfoCollection videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                    if (videoDevices.Count == 0)
                    {
                        TextLogUtil.Info("无摄像头");
                        return new CommonResult() { result = false, message = "无摄像头" };
                    }
                    string monikerString = null;
                    if (videoDevices.Count > 1)
                    {
                        foreach (FilterInfo item in videoDevices)
                        {
                            if (item.Name.ToLower().Contains("usb"))
                            {
                                monikerString = item.MonikerString;
                                break;
                            }
                        }
                        if (monikerString == null)
                        {
                            TextLogUtil.Info("无人脸识别摄像头");
                            return new CommonResult() { result = false, message = "无摄像头" };
                        }
                    }
                    else
                    {
                        monikerString = videoDevices[0].MonikerString;
                    }
                    videoSource = new VideoCaptureDevice(monikerString);
                    videoSource.SignalToStop();
                }

                videoSource.Start();
                videoSource.NewFrame += video_NewFrame;
            }
            catch (Exception ep)
            {
                return new CommonResult() { result = false, message = ep.Message };
            }
            return new CommonResult(){result = true};
        }
        public CommonResult Stop()
        {
            BitMapCallBack = null;
            CallBackAction = null;
            try
            {
                if (videoSource != null)
                {
                    videoSource.NewFrame -= video_NewFrame;
                    videoSource.SignalToStop();
                    videoSource = null;
                }
            }
            catch (Exception e)
            {
                return new CommonResult(){result = false,message = e.Message};
            }
            return new CommonResult(){result = true};
        }

        public bool IsBuys = false;
        private void video_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            var bitmap = (Bitmap)eventArgs.Frame.Clone();

            BitMapCallBack?.Invoke(bitmap);

            if (IsBuys)
            {
                bitmap.Dispose();
                return;
            }
            IsBuys = true;

            byte[] bitmapBytes = ImageToBytes(bitmap);
            Task.Run(() =>
            {
                try
                {
                    if (bitmapBytes == null)
                    {
                        return;
                    }
                    var result = BaiduFaceApiHelper.GetInstance().Verification(bitmapBytes);
                    if (result.result)
                    {
                        //判断人脸是否存在
                        var temp = BaiduFaceApiHelper.GetInstance().Contrast(bitmapBytes);
                        if (!temp.result)
                        {
                            //添加人脸
                            temp = BaiduFaceApiHelper.GetInstance().EditUserWithDB(bitmapBytes);
                        }
                        CallBackAction?.Invoke(bitmapBytes);
                        Stop();
                    }
                }
                catch (Exception ep)
                {
                    MessageBox.Show(ep.Message);
                    IsBuys = false;
                    return;
                }
                IsBuys = false;
            });

            bitmap.Dispose();
        }
        public static byte[] ImageToBytes(Bitmap image)
        {
            try
            {
                if (image == null)
                {
                    return null;
                }
                MemoryStream ms = new MemoryStream();
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                byte[] bytes = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(bytes, 0, (int)ms.Length);
                ms.Close();
                return bytes;
            }
            catch (Exception ep)
            {
                return null;
            }
        }

    }
}
