﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;

namespace NSJL.Plugin.Third.ThirdParty
{
    public class HighBeatHelper
    {
        private static readonly object locks = new object();
        private static HighBeatHelper manage;
        public static HighBeatHelper GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new HighBeatHelper());
            }
        }
        public AxActiveXLib.AxActiveX axActiveX { get; set; }

        public System.Windows.Forms.Integration.WindowsFormsHost host = new System.Windows.Forms.Integration.WindowsFormsHost();
        public HighBeatHelper()
        {
            axActiveX = new AxActiveXLib.AxActiveX();
        }

        public CommonResult Start(DockPanel dockPanel1)
        {
            try
            {
                axActiveX.BeginInit();
                host.Child = axActiveX;
                dockPanel1.Children.Clear();
                dockPanel1.Children.Add(host);
                axActiveX.EndInit();

                var count = axActiveX.GetDeviceCount();
                if (count <= 0)
                {
                    return new CommonResult(){result = false,message = "没有可用的高拍仪设备"};
                }
                var fopen = axActiveX.OpenDeviceEx(0);
                if (!fopen)
                {
                    return new CommonResult() { result = false, message = "打开设备失败" };
                }
                var srsult = axActiveX.SetResolution(2, 1024, 768);
                if (!srsult)
                {
                    TextLogUtil.Info("设置分辨率失败");
                }
                //打开视频
                var temp = axActiveX.OpenVideo();
                if (!temp)
                {
                    return new CommonResult(){result = false,message = "打开视频失败"};
                }
                return new CommonResult(){result = true};
            }
            catch (Exception e)
            {
                return new CommonResult(){result = false,message = e.Message};
            }
        }
        public bool Photograph(string path)
        {
            return axActiveX.Capture(path);
        }
        public void Stop(DockPanel dockPanel1)
        {
            try
            {
                //关闭视频
                axActiveX.CloseVideo();
                //关闭设备
                axActiveX.CloseDevice();
                host.Child = null;
                dockPanel1.Children.Clear();
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }
        }



    }
}
