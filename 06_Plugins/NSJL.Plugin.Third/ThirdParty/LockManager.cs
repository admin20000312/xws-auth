﻿using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;
using NSJL.Plugin.Third.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DJ.ZEF.ThirdParty
{
    public class LockManager
    {
        private static readonly object locks = new object();
        private static LockManager manage;
        public static LockManager GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new LockManager());
            }
        }

        private SerialPortHelp sp = null;
        private CachedBuffer cache=new CachedBuffer();

        // 秒数  * 10的值
        private int timeOut { get; set; } = 20;
        // 当前值/10  为发送秒数间隔
        private int timeOutSpace { get; set; } = 10;

        public CommonResult Start(string portName,int baudRate)
        {
            cache.Clear();
            //var data=SelfUtil.GetXmlWithPath<DJCabinet>("config/DJCabinetCfg.xml");
            if (sp == null)
            {
                sp = new SerialPortHelp();
                sp.CallBackAction = DataReceived;
                return sp.Start(portName, baudRate);
            }
            return new CommonResult(){result = false,message = "已初始化"};
        }
        public DateTime LasTime { get; set; } = DateTime.Now;
        private static readonly object obj=new object();
        public void OpenBox(int boxid)
        {
            var bytes = new byte[] { 0x55, (byte)boxid, 0xa1, 0x5f, 0x00 };

            //计算校验位
            var temp = SelfUtil.XorCheck(bytes);
            var newbyte = new byte[bytes.Length + 1];
            Array.Copy(bytes, newbyte, bytes.Length);
            newbyte[bytes.Length] = temp;

            lock (obj)
            {
                if (DateTime.Now.AddSeconds(-1) < LasTime)
                {
                    Thread.Sleep(800);
                }
                sp.SendByte(newbyte);
                LasTime = DateTime.Now;
            }
        }
        public void CheckBox(int boxid,Action<bool?> callback)
        {
            callBackResult = null;
            cache.Clear();

            var bytes = new byte[] { 0x55, (byte)boxid, 0xa2, 0x00, 0x00 };

            //计算校验位
            var temp = SelfUtil.XorCheck(bytes);
            var newbyte = new byte[bytes.Length + 1];
            Array.Copy(bytes, newbyte, bytes.Length);
            newbyte[bytes.Length] = temp;

            Task.Factory.StartNew(() =>
            {
                var flag = 0;
                while (callBackResult == null)
                {
                    if (flag % timeOutSpace == 0)
                    {
                        lock (obj)
                        {
                            if (DateTime.Now.AddSeconds(-1) < LasTime)
                            {
                                Thread.Sleep(800);
                            }
                            sp.SendByte(newbyte);
                            LasTime = DateTime.Now;
                        }
                    }
                    Thread.Sleep(100);
                    if (flag > timeOut)
                    {
                        break;
                    }
                    flag++;
                }
                if (callBackResult != null && callBackResult.Length == 6)
                {
                    if (callBackResult[4] == 0x70)
                    {
                        callback(true);
                        callBackResult = null;
                        return;
                    }
                    if (callBackResult[4] == 0x7F)
                    {
                        callback(false);
                        callBackResult = null;
                        return;
                    }
                }
                callback(null);
                callBackResult = null;
            });
        }
        private byte[] callBackResult = null;
        public void DataReceived(byte[] buffer)
        {
            cache.Write(buffer, 0, buffer.Length);
            var temp=cache.ReadDataWithPlatform();
            if (temp == null)
            {
                return;
            }
            callBackResult=new byte[temp.Length];
            Array.Copy(temp, callBackResult, temp.Length);
        }


        public void SetStart(int boxid)
        {
            var bytes = new byte[] { 0x55, 0x00, 0xa6, (byte)boxid, 0x00 };

            //计算校验位
            var temp = SelfUtil.XorCheck(bytes);
            var newbyte = new byte[bytes.Length + 1];
            Array.Copy(bytes, newbyte, bytes.Length);
            newbyte[bytes.Length] = temp;

            sp.SendByte(newbyte);
        }
        public void SetEnd(int boxid)
        {
            var bytes = new byte[] { 0x55, 0x00, 0xa4, (byte)boxid, 0x00 };

            //计算校验位
            var temp = SelfUtil.XorCheck(bytes);
            var newbyte = new byte[bytes.Length + 1];
            Array.Copy(bytes, newbyte, bytes.Length);
            newbyte[bytes.Length] = temp;

            sp.SendByte(newbyte);
        }
        public void Close()
        {
            if (sp != null)
            {
                sp.Close();
	            sp = null;
            }
        }
    }
}
