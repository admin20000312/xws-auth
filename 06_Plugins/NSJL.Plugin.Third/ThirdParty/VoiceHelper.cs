﻿using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;

namespace DJ.ZEF.ThirdParty
{
    public class VoiceHelper
    {
        private static readonly object locks = new object();
        private static VoiceHelper manage;
        public static VoiceHelper GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new VoiceHelper());
            }
        }
        private SpeechSynthesizer speechPlayer=new SpeechSynthesizer();

        public void Start(string str)
        {
            try
            {
                speechPlayer.SpeakAsyncCancelAll();
                speechPlayer.SpeakAsync(str);
            }
            catch(Exception ex)
            {
                TextLogUtil.Info(ex.Message);
            }
        }
    }
}
