﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;

namespace NSJL.Plugin.Third.ThirdParty
{
    public class LockManager2
    {
        private static readonly object locks = new object();
        private static LockManager2 manage;
        public static LockManager2 GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new LockManager2());
            }
        }
        private SerialPortHelp sp = null;
        private CachedBuffer cache = new CachedBuffer();

        // 秒数  * 10的值
        private int timeOut { get; set; } = 20;
        // 当前值/10  为发送秒数间隔
        private int timeOutSpace { get; set; } = 10;
        public CommonResult Start(string portName, int baudRate)
        {
            //var data=SelfUtil.GetXmlWithPath<DJCabinet>("config/DJCabinetCfg.xml");
            if (sp == null)
            {
                sp = new SerialPortHelp();
                sp.CallBackAction = DataReceived;
                return sp.Start(portName, baudRate);
            }
            return new CommonResult() { result = false, message = "已初始化" };
        }
        public void OpenBox(int sboxid)
        {
            var boxid = Convert.ToInt16(sboxid);

            var boxty=BitConverter.GetBytes(boxid);
            Array.Reverse(boxty);
            var bytes = new byte[] { 0x5c,0x31, boxty[0], boxty[1] };

            //计算校验位
            var temp = SelfUtil.XorCheck(bytes);
            var newbyte = new byte[bytes.Length + 1];
            Array.Copy(bytes, newbyte, bytes.Length);
            newbyte[bytes.Length] = temp;

            sp.SendByte(newbyte);
        }

        private byte[] callBackResult = null;
        public void DataReceived(byte[] buffer)
        {
            cache.Write(buffer, 0, buffer.Length);
            var temp = cache.ReadDataWithPlatformNew();
            if (temp == null)
            {
                return;
            }
            callBackResult = new byte[temp.Length];
            Array.Copy(temp, callBackResult, temp.Length);
        }

        public void SetStartAndEnd(int sstart, int send)
        {
            var start = Convert.ToInt16(sstart);
            var end = Convert.ToInt16(send);


            var bstart = BitConverter.GetBytes(start);
            Array.Reverse(bstart);
            var bend = BitConverter.GetBytes(end);
            Array.Reverse(bend);
            var bytes = new byte[] { 0x5c, 0x34, bstart[0],bstart[1],bend[0],bend[1] };

            //计算校验位
            var temp = SelfUtil.XorCheck(bytes);
            var newbyte = new byte[bytes.Length + 1];
            Array.Copy(bytes, newbyte, bytes.Length);
            newbyte[bytes.Length] = temp;

            sp.SendByte(newbyte);
        }
        public void Close()
        {
            if (sp != null)
            {
                sp.Close();
                sp = null;
            }
        }

        public void CheckBox(int sboxid, Action<bool?> callback)
        {
            var boxid = Convert.ToInt16(sboxid);

            callBackResult = null;
            cache.Clear();

            var boxty = BitConverter.GetBytes(boxid);
            Array.Reverse(boxty);
            var bytes = new byte[] { 0x5c, 0x3c, boxty[0], boxty[1] };

            //计算校验位
            var temp = SelfUtil.XorCheck(bytes);

            var newbyte = new byte[bytes.Length + 1];
            Array.Copy(bytes, newbyte, bytes.Length);
            newbyte[bytes.Length] = temp;

            Task.Factory.StartNew(() =>
            {
                var flag = 0;
                while (callBackResult == null)
                {
                    if (flag % timeOutSpace == 0)
                    {
                        sp.SendByte(newbyte);
                    }
                    Thread.Sleep(100);
                    if (flag > timeOut)
                    {
                        break;
                    }
                    flag++;
                }
                if (callBackResult != null && callBackResult.Length == 6)
                {
                    var results = callBackResult[4];

                    if (results == 0)
                    {
                        callback(true);
                        callBackResult = null;
                        return;
                    }
                    if (results == 1)
                    {
                        callback(false);
                        callBackResult = null;
                        return;
                    }
                }
                callback(null);
                callBackResult = null;
            });
        }
        public void CheckBoxGoods(int sboxid, Action<bool?> callback)
        {
            var boxid = Convert.ToInt16(sboxid);

            callBackResult = null;
            cache.Clear();

            var boxty = BitConverter.GetBytes(boxid);
            Array.Reverse(boxty);
            var bytes = new byte[] { 0x5c, 0x3d, boxty[0], boxty[1] };

            //计算校验位
            var temp = SelfUtil.XorCheck(bytes);

            var newbyte = new byte[bytes.Length + 1];
            Array.Copy(bytes, newbyte, bytes.Length);
            newbyte[bytes.Length] = temp;

            Task.Factory.StartNew(() =>
            {
                var flag = 0;
                while (callBackResult == null)
                {
                    if (flag % timeOutSpace == 0)
                    {
                        sp.SendByte(newbyte);
                    }
                    Thread.Sleep(100);
                    if (flag > timeOut)
                    {
                        break;
                    }
                    flag++;
                }
                if (callBackResult != null && callBackResult.Length == 6)
                {
                    var results = callBackResult[4];

                    if (results == 0)
                    {
                        callback(true);
                        callBackResult = null;
                        return;
                    }
                    if (results == 1)
                    {
                        callback(false);
                        callBackResult = null;
                        return;
                    }
                }
                callback(null);
                callBackResult = null;
            });
        }



        #region 无用  以前的测试 返回多个状态
        //检测箱门状态   开是true   关是  false
        public void CheckBox3(Int16 boxid, Action<bool?> callback)
        {
            callBackResult = null;
            cache.Clear();

            var boxty = BitConverter.GetBytes(boxid);
            Array.Reverse(boxty);
            var bytes = new byte[] { 0x5c, 0x32, boxty[0], boxty[1] };

            //计算校验位
            var temp = SelfUtil.XorCheck(bytes);
            var newbyte = new byte[bytes.Length + 1];
            Array.Copy(bytes, newbyte, bytes.Length);
            newbyte[bytes.Length] = temp;

            Task.Factory.StartNew(() =>
            {
                var flag = 0;
                while (callBackResult == null)
                {
                    if (flag % timeOutSpace == 0)
                    {
                        sp.SendByte(newbyte);
                    }
                    Thread.Sleep(100);
                    if (flag > timeOut)
                    {
                        break;
                    }
                    flag++;
                }
                if (callBackResult != null && callBackResult.Length == 10)
                {
                    byte[] temps1 = { callBackResult[3], callBackResult[2] };
                    var starts = BitConverter.ToInt16(temps1, 0);

                    byte[] temps2 = { callBackResult[5], callBackResult[4] };
                    var ends = BitConverter.ToInt16(temps2, 0);

                    //1是关闭  0 开
                    var type1 = Convert.ToString(callBackResult[6], 2).PadLeft(8, '0');
                    var type2 = Convert.ToString(callBackResult[7], 2).PadLeft(8, '0');
                    var type3 = Convert.ToString(callBackResult[8], 2).PadLeft(8, '0');
                    var all = type3 + type2 + type1;

                    char[] arr = all.ToCharArray();
                    Array.Reverse(arr);
                    var arrtemp = new string(arr);
                    //var boxtemp = boxid % 24;  //24箱门一组

                    var boxtemp = boxid - starts + 1;
                    var resultt = arrtemp.Substring(boxtemp - 1, 1);

                    if (resultt == "0")
                    {
                        callback(true);
                        callBackResult = null;
                        return;
                    }
                    if (resultt == "1")
                    {
                        callback(false);
                        callBackResult = null;
                        return;
                    }
                }
                callback(null);
                callBackResult = null;
            });
        }
        //检测是否存物  true1有物品   false0没物品
        public void CheckBoxGoods3(Int16 boxid, Action<bool?> callback)
        {
            callBackResult = null;
            cache.Clear();

            var boxty = BitConverter.GetBytes(boxid);
            Array.Reverse(boxty);
            var bytes = new byte[] { 0x5c, 0x33, boxty[0], boxty[1] };

            //计算校验位
            var temp = SelfUtil.XorCheck(bytes);
            var newbyte = new byte[bytes.Length + 1];
            Array.Copy(bytes, newbyte, bytes.Length);
            newbyte[bytes.Length] = temp;

            Task.Factory.StartNew(() =>
            {
                var flag = 0;
                while (callBackResult == null)
                {
                    if (flag % timeOutSpace == 0)
                    {
                        sp.SendByte(newbyte);
                    }
                    Thread.Sleep(100);
                    if (flag > timeOut)
                    {
                        break;
                    }
                    flag++;
                }
                if (callBackResult != null && callBackResult.Length == 10)
                {
                    //1有物品  0 没物品
                    var type1 = Convert.ToString(callBackResult[6], 2).PadLeft(8, '0');
                    var type2 = Convert.ToString(callBackResult[7], 2).PadLeft(8, '0');
                    var type3 = Convert.ToString(callBackResult[8], 2).PadLeft(8, '0');
                    var all = type3 + type2 + type1;

                    char[] arr = all.ToCharArray();
                    Array.Reverse(arr);
                    var arrtemp = new string(arr);

                    var boxtemp = boxid % 24;  //24箱门一组

                    var resultt = arrtemp.Substring(boxtemp - 1, 1);

                    if (resultt == "1")
                    {
                        callback(true);
                        callBackResult = null;
                        return;
                    }
                    if (resultt == "0")
                    {
                        callback(false);
                        callBackResult = null;
                        return;
                    }
                }
                callback(null);
                callBackResult = null;
            });
        }
        #endregion
    }
}
