﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Exceptions;
using Aliyun.Acs.Core.Http;
using Aliyun.Acs.Core.Profile;
using NSJL.Framework.Utils;

namespace DJ.ZEF.ThirdParty
{
    public class SendMessage
    {
        public string regionId = "cn-hangzhou";
        public string accessKeyId = "LTAI4GAeykeQmj2ddjBSF7UF";
        public string accessSecret = "8h0MoFfND5YYAelWc71FXm91sIXwr5";
        public string signName = "杭州东捷智能科技有限公司";

        private static readonly object locks = new object();
        private static SendMessage manage;
        public static SendMessage GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new SendMessage());
            }
        }

        public SendMessage()
        {
        }
        //SMS_216427717
        public bool SendMessageWithData<T>(string mobile,T data,string templateCode= "SMS_216427717")
        {
            IClientProfile profile = DefaultProfile.GetProfile(regionId, accessKeyId, accessSecret);
            DefaultAcsClient client = new DefaultAcsClient(profile);
            CommonRequest request = new CommonRequest();
            request.Method = MethodType.POST;
            request.Domain = "dysmsapi.aliyuncs.com";
            request.Version = "2017-05-25";
            request.Action = "SendSms";
            request.AddQueryParameters("PhoneNumbers", mobile);
            request.AddQueryParameters("SignName", signName);     //模板签名
            request.AddQueryParameters("TemplateCode", templateCode); //模板编号
            request.AddQueryParameters("TemplateParam", new JavaScriptSerializer().Serialize(data));    //参数json   "{\"code\":\"1111\"}"
            try
            {
                CommonResponse response = client.GetCommonResponse(request);
                if (!response.Data.Contains("OK"))
                {
                    TextLogUtil.Info(response.Data);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (ServerException ex)
            {
                //throw new Exception(ex.ErrorMessage);
            }
            catch (ClientException ex)
            {
                //throw new Exception(ex.ErrorMessage);
            }
            return false;
        }
        //新版
        public bool SendMessageWithNew<T>(string mobile, T data, string templateCode = "SMS_216427717")
        {
            AlibabaCloud.OpenApiClient.Models.Config config = new AlibabaCloud.OpenApiClient.Models.Config
            {
                AccessKeyId = accessKeyId,
                AccessKeySecret = accessSecret,
            };
            config.Endpoint = "dysmsapi.aliyuncs.com";
            var client = new AlibabaCloud.SDK.Dysmsapi20170525.Client(config);

            AlibabaCloud.SDK.Dysmsapi20170525.Models.SendSmsRequest sendSmsRequest = new AlibabaCloud.SDK.Dysmsapi20170525.Models.SendSmsRequest();
            sendSmsRequest.SignName = signName;
            sendSmsRequest.PhoneNumbers = mobile;
            sendSmsRequest.TemplateCode = templateCode;
            sendSmsRequest.TemplateParam = new JavaScriptSerializer().Serialize(data);
            try
            {
                var res = client.SendSms(sendSmsRequest);
                TextLogUtil.Info(res.Body.Message);
                if (res.Body.Message.Contains("OK"))
                {
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                TextLogUtil.Info("发送短信失败" + e.Message);
                return false;
            }
        }
    }
}
