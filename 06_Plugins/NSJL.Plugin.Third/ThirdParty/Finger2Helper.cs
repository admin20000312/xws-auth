﻿using libzkfpcsharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using NSJL.Framework.Utils;

namespace DJ.ZEF.ThirdParty
{
    public class Finger2Helper
    {
        private static readonly object locks = new object();
        private static Finger2Helper manage;
        public static Finger2Helper GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new Finger2Helper());
            }
        }

        public IntPtr mDevHandle;
        public void Start()
        {
            var ret = zkfp2.Init();
            if (ret != zkfperrdef.ZKFP_ERR_OK)
            {
                TextLogUtil.Info("初始化指纹器设备失败!, 错误码为:" + ret + " !");
                return;
            }
            var deviceCount = zkfp2.GetDeviceCount();
            if (deviceCount <= 0)
            {
                zkfp2.Terminate();
                TextLogUtil.Info("找不到指纹器");
                return;
            }

            #region 使用指纹自带的 数据库
            //var dbHandle = zkfp2.DBInit();
            //对比指纹
            //ret = zkfp2.DBIdentify(dbHandle, fingerprint, ref iFid, ref iScore);
            //3次指纹合成一次
            // zkfp2.DBMerge(dbHandle, temp1, temp2, temp3, regTemp, ref regTempLen);
            //添加指纹操作
            //int ret = zkfp2.DBAdd(dbHandle, iFid, fingerFrint);
            //            if (ret != 0)
            //            {
            //                LogHelper.Record(string.Format("ZKDB添加指纹出错，错误码 = {0}", iFid), "指纹错误！");
            //                return -1;
            //            }
            //删除指纹操作
            //int ret = zkfp2.DBDel(dbHandle, iFid);
            #endregion

            mDevHandle=zkfp2.OpenDevice(0);


            int width = 0;
            int height = 0;

            byte[] paramValue = new byte[4];
            int size = 4;
            zkfp2.GetParameters(mDevHandle, 1, paramValue, ref size);
            zkfp2.ByteArray2Int(paramValue, ref width);
            size = 4;
            zkfp2.GetParameters(mDevHandle, 2, paramValue, ref size);
            zkfp2.ByteArray2Int(paramValue, ref height);

            var imageBuf = new byte[width * height]; // 1024 * 1024
            var tempBuf = new byte[1024];
            int sizes = 0;
            //获取指纹图片和模板特征值
            ret = zkfp2.AcquireFingerprint(mDevHandle, imageBuf, tempBuf, ref sizes);
            if (ret == zkfp.ZKFP_ERR_OK)
            {
                MemoryStream ms = new MemoryStream();
                BitmapFormat.GetBitmap(imageBuf, width, height, ref ms);
                Bitmap bmp = new Bitmap(ms);
                

                //模板
                var sss = Convert.ToBase64String(tempBuf);

                bmp.Save(@"C:\Users\Administrator\Desktop\1.jpg",ImageFormat.Jpeg);

                //byte[] temp1 = zkfp.Base64String2Blob();
                //var score = zkfp2.DBMatch(mDevHandle, temp1, fingerprint);
                //if (score >= 40)
                //{

                //}
            }
        }

        public void Stop()
        {
            zkfp2.CloseDevice(mDevHandle);
        }


    }
}
