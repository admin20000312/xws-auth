﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DomainModel.Background.Client
{
    public class GetUserInfo : INotifyPropertyChanged
    {
        private string _Name = string.Empty;
        //private string _Code = string.Empty;
        private string _HeadPic = string.Empty;
        private string _FaceId = string.Empty;
        private DateTime? _CreateTime = null;
        private string _Password = string.Empty;
        private string _Mobile = string.Empty;

        private string _Role = string.Empty;
        private string _RoleName = string.Empty;
        public string Role { get { return _Role; } set { UpdateProperty(ref _Role, value); } }
        public string RoleName { get { return _RoleName; } set { UpdateProperty(ref _RoleName, value); } }

        public string Id { get; set; }
        public string RoleId { get; set; }
        public string Features { get; set; }
        //姓名
        public string Name { get{return _Name;} set { UpdateProperty(ref _Name, value); } }
        ////卡号
        //public string Code { get { return _Code; } set { UpdateProperty(ref _Code, value); } }
        //头像 base64
        public string HeadPic { get { return _HeadPic; } set { UpdateProperty(ref _HeadPic, value); } }
        public string FaceId { get { return _FaceId; } set { UpdateProperty(ref _FaceId, value); } }
        //创建时间
        public DateTime? CreateTime { get { return _CreateTime; } set { UpdateProperty(ref _CreateTime, value); } }
        public string Password { get { return _Password; } set { UpdateProperty(ref _Password, value); } }
        public string Mobile { get { return _Mobile; } set { UpdateProperty(ref _Mobile, value); } }

        private void UpdateProperty<T>(ref T properValue, T newValue, [CallerMemberName] string propertyName = "")
        {
            if (object.Equals(properValue, newValue))
            {
                return;
            }
            properValue = newValue;

            OnPropertyChanged(propertyName);
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }



    public class GetGoodsList : INotifyPropertyChanged
    {
        private string _GoodsId = string.Empty;
        private string _GoodsName = string.Empty;
        private int? _StockNum ;
        private int? _StandardNum ;
        public string Id { get; set; }
        public string CabName { get; set; }
        public string BoxNum { get; set; }
        public string Position { get; set; }
        public string GoodsId { get { return _GoodsId; } set { UpdateProperty(ref _GoodsId, value); } }
        public string GoodsName { get { return _GoodsName; } set { UpdateProperty(ref _GoodsName, value); } }
        public int? StockNum { get { return _StockNum; } set { UpdateProperty(ref _StockNum, value); } }
        public int? StandardNum { get { return _StandardNum; } set { UpdateProperty(ref _StandardNum, value); } }



        private void UpdateProperty<T>(ref T properValue, T newValue, [CallerMemberName] string propertyName = "")
        {
            if (object.Equals(properValue, newValue))
            {
                return;
            }
            properValue = newValue;

            OnPropertyChanged(propertyName);
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
