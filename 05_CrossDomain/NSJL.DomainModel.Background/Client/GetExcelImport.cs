﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DomainModel.Background.Client
{
    public class GetExcelImport
    {
        public string CabName { get; set; }
        public int BoxNum { get; set; }
        public string Position { get; set; }
        public string GoodsName { get; set; }
        public string GoodsId { get; set; }
        public string matnr { get; set; }
        public string charg { get; set; }
        public int? menge { get; set; }
        public string werks { get; set; }
        public string meins { get; set; }
        public string rsnum { get; set; }
        public string rspos { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? StandardNum { get; set; }
    }
}
