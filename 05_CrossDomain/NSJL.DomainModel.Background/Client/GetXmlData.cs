﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DomainModel.Background.Client
{
    public class GetXmlData
    {
        public string id { get; set; }
        public List<atts> atts { get; set; }
    }
    public class atts
    {
        public string fileUrl { get; set; }
    }

    public class Excel
    {
        public string 物料编号 { get; set; }
        public string 物料描述 { get; set; }
        public string 批次 { get; set; }
        public string 数量 { get; set; }
        public string 库存地点 { get; set; }
    }

    public class Excel2
    {
        public string 物料编号 { get; set; }
        public string 物料描述 { get; set; }
        public string 批次 { get; set; }
        public string 数量 { get; set; }
        public string 库存地点 { get; set; }
        public string 库龄 { get; set; }
    }



    public class BuKu
    {
        public List<userdata> userdata { get; set; }=new List<userdata>();
        public string systemid { get; set; } = "534AC75C2E8AC3E3FE7BC32BB8C6E34A";
        public string checkcode { get; set; } = "26d55ad283aa400af464c76d713c07ad";
    }
    public class userdata
    {
        public List<userdataitem> item { get; set; }=new List<userdataitem>();
    }

    public class userdataitem
    {
        public string matnr { get; set; }
        public string werks { get; set; }
        public string charg { get; set; }
        public string bwart { get; set; }
        public string menge { get; set; }
        public string meins { get; set; }
        public string lgort { get; set; }

        public string umlgo { get; set; }
        public string lgpla { get; set; }
        public string lgpla1 { get; set; }
        public string rsnum { get; set; }
        public string rspos { get; set; }

        public string kostl { get; set; }

        public string mblnr { get; set; }
    }

}
