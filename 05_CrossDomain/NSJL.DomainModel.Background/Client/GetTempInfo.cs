﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DomainModel.Background.Client
{
    public class GetTempInfo
    {
        public string Id { get; set; }
        public DateTime CreateTime { get; set; }

        public string matnr { get; set; }   //物料编码
        public string werks { get; set; }   //工厂
        public string charg { get; set; }   //批次
        public string meins { get; set; }   //单位
        public string rsnum { get; set; }
        public string rspos { get; set; }


        public string bwart { get; set; }
        public int menge { get; set; }
        public string lgort { get; set; }
        public string umlgo { get; set; }
        public string lgpla { get; set; }
        public string mblnr { get; set; }
        public string sgtxt { get; set; }


        public string CabName { get; set; }
        public int? BoxNum { get; set; }
        public string Position { get; set; }
        public string GoodsName { get; set; }
        public string GoodsId { get; set; }
    }
}
