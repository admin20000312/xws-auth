﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace NSJL.DomainModel.Background.Client
{
    
    [XmlRoot("OtData")]
    public class OtData : List<item2>
    {
        //[XmlArray("items")]
        //public List<item> items { get; set; }
    }
    [Serializable, XmlType("item")]
    public class item2
    {
        public string Rsnum { get; set; }
        public string Rspos { get; set; }
        public string Werks { get; set; }   //工厂
        public string Matnr { get; set; }   //物料编码
        public string Charg { get; set; }   //批次
        public string Lgort { get; set; }
        public string Umlgo { get; set; }
        public string Bwart { get; set; }
        public decimal? Menge { get; set; }   //数量
        public string Meins { get; set; }
        public string BudatMkpf { get; set; }
        public string Mblnr { get; set; }
        public string Sgtxt { get; set; }
        public string Wgbez { get; set; }
        public string Maktx { get; set; }
    }

    [XmlRoot("OT_DATA")]
    public class OT_DATA : List<item>
    {
        //[XmlArray("items")]
        //public List<item> items { get; set; }
    }
    //[Serializable, XmlType("item")]
    public class item
    {
        //----------补库
        public string RSNUM { get; set; }
        public string RSPOS { get; set; }
        public string WERKS { get; set; }
        public string MATNR { get; set; }
        public string CHARG { get; set; }
        public string LGORT { get; set; }
        public string UMLGO { get; set; }
        public string BWART { get; set; }
        public string MENGE { get; set; }
        public string MEINS { get; set; }
        public string BUDAT_MKPF { get; set; }
        //----------------

        //--------物料类别
        //public string MATNR { get; set; }
        public string MATKL { get; set; }
        public string WGBEZ { get; set; }
        //----------------

        //-----------成本中心
        public string KOKRS { get; set; }
        public string KOSTL { get; set; }
        public string KTEXT { get; set; }
        public string DATAB { get; set; }
        public string DATBI { get; set; }
        //----------------

        public string MBLNR { get; set; }
    }

        

    
}
