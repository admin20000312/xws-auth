﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DomainModel.Background.Client
{
    public class GetBoxInfo
    {
        public string Id { get; set; }
        //箱门编号
        public int BoxNum { get; set; }
        //是否锁定   默认false
        public bool? IsLock { get; set; }
        //是否单警柜子
        public int? IsSingle { get; set; }
        //物品存入的时间
        public DateTime? CreateTime { get; set; }
        //领取的时间
        public DateTime? UpdateTime { get; set; }
    }


    public class QueueData
    {
        public int BoxNum { get; set; }
        public string UserName { get; set; }
        public string Mode { get; set; }
    }


}
