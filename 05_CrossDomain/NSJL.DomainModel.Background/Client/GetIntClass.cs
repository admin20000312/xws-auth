﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DomainModel.Background.Client
{
    public class GetIntClass
    {
        public string corpCode { get; set; }
        public string deviceCode { get; set; }
        public int pageNum { get; set; } = 1;
        public int pageSize { get; set; } = 200;
    }


    public class GetCallBack<T>
    {
        public int? status { get; set; }
        public bool? success { get; set; }
        public string msg { get; set; }
        public int? total { get; set; }
        public T data { get; set; }
    }
    public class SysTime
    {
        public string systemTime { get; set; }
    }

    public class GetInt2Class<T>
    {
        public string corpCode { get; set; }
        public string deviceCode { get; set; }
        public string userCode { get; set; }
        public T data { get; set; }
    }
    public class GetReport
    {
        //0：取出；1：存入
        public int status { get; set; }
        public string epc { get; set; }
        public int cellID { get; set; }
    }

    public class CallBack
    {
        public int id { get; set; }
        public string userCode { get; set; }
        public string userName { get; set; }
        public string cellId { get; set; }
        public string cardId { get; set; }
        public string faceInfo { get; set; }
        public string fingerPrint { get; set; }
        public int userType { get; set; }


        public string name { get; set; }
        public string epc { get; set; }
    }

}
