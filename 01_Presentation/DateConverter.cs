﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace DJ.Pages
{
    [ValueConversion(typeof(string), typeof(BitmapImage))]
    public class DateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value==null || string.IsNullOrWhiteSpace(value.ToString()))
            {
                var filepath = "pack://application:,,,/Image/组 3@2x.png";
                return filepath;
            }
            try
            {
                var bytes = System.Convert.FromBase64String(value.ToString());
                return ShowSelectedIMG(bytes);
            }
            catch (Exception e)
            {
                var filepath = "pack://application:,,,/Image/组 3@2x.png";
                return filepath;
            }
        }
        private BitmapImage ShowSelectedIMG(byte[] img)
        {
            try
            {
                if (img != null)
                {
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(img);
                    ms.Seek(0, System.IO.SeekOrigin.Begin);
                    BitmapImage newBitmapImage = new BitmapImage();
                    newBitmapImage.BeginInit();
                    newBitmapImage.StreamSource = ms;
                    newBitmapImage.EndInit();
                    return newBitmapImage;
                }
                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
