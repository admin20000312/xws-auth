﻿using NSJL.Biz.Background.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DJ.Dialog;
using NSJL.Framework.Utils;
using DJ.ZEF.ThirdParty;
using DJ.Clients;

namespace DJ
{
    /// <summary>
    /// Login.xaml 的交互逻辑
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            //WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
            ComWindow.Start(this);
        }
        //登录
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Label1.Content = "";
            var username = TextBox1.Text;
            var passowrd = PasswordBox1.Password;
            Task.Run(() =>
            {
                try
                {
                    var biz = new CommBiz();
                    var result = biz.Login(username, passowrd);
                    if (result.result)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            Main.UserName = TextBox1.Text;
                            try
                            {
                                Main main = Main.GetInstance();
                                main.ShowDialog();
                                this.Close();
                            }
                            catch (Exception exception)
                            {
                            }
                        });
                    }
                    else
                    {
                        Dispatcher.Invoke(() =>
                        {
                            //MessageDialog.ShowDialog(result.message);
                            //MessageBox.Show(result.message);
                            Label1.Content = result.message;
                        });
                    }
                }
                catch (Exception exception)
                {
                    TextLogUtil.Info(exception.Message);
                }
            });
        }
        //返回主页
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TextBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }
    }
}
