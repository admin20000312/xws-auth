﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using DJ.Clients;
using DJ.Dialog;
using DJ.UserControls;
using DJ.ZEF.ThirdParty;
using NSJL.Biz.Background.Client;
using NSJL.Biz.Background.Quartzs;
using NSJL.DAL.DataModel.Entities;
using NSJL.DomainModel.Background.Client;
using NSJL.Framework.Utils;
using NSJL.Plugin.Third.ThirdParty;

namespace DJ
{
    /// <summary>
    /// Client.xaml 的交互逻辑
    /// </summary>
    public partial class Client : Window
    {
        public static string FileName= AppDomain.CurrentDomain.BaseDirectory + "authorization.dll";
        public int? Minute = null;
        public string AuthCode { get; set; }

        public MessageQueue que=new MessageQueue();
        public class AntBox
        {
            public int BoxNum { get; set; }
            public List<string> AntList { get; set; }
        }
        public List<AntBox> ClientAntList { get; set; }=new List<AntBox>();
        public Client()
        {
            var aaaa = GetCpuID();
            if (aaaa != null)
            {
                AuthCode = aaaa;
            }
            else
            {
                AuthCode= GetHardDiskID();
            }

            #region 读取授权文件  时间
            try
            {
                if (File.Exists(FileName))
                {
                    var content = File.ReadAllText(FileName);
                    var temp =Convert.FromBase64String(content);
                    var temp222=Encoding.UTF8.GetString(temp);

                    var arr = temp222.Split('#');
                    var num = arr[0].ToInt32();//52557000
                    var time = arr[1].ToDateTime();
                    var authss = arr[2].Trim();
                    if (DateTime.Now < time && authss == AuthCode)
                    {
                        Minute = num - 1;
                        DispatcherTimer timers = new DispatcherTimer();
                        timers.Interval = new TimeSpan(0, 1, 0);
                        timers.Tick += ((a, b) =>
                        {
                            if (Minute != null)
                            {
                                Minute = Minute - 1;
                                var tiansss = (Minute.Value / 60 / 24) + 1;
                                if (tiansss <= 0)
                                {
                                    tiansss = 0;
                                }else if (Minute == null || Minute <= 0)
                                {
                                    tiansss = 0;
                                }
                                auth.Content = "授权倒计时:"+ tiansss + "天";

                                //写入授权文件
                                var ccc = Minute + "#" + DateTime.Now.AddDays(30).ToString("yyyy-MM-dd HH:mm:ss")+"#" + AuthCode;
                                var base64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(ccc));
                                File.WriteAllText(FileName, base64);
                            }
                        });
                        timers.IsEnabled = true;
                        timers.Start();
                    }
                }
            }
            catch (Exception e)
            {
            }
            #endregion


            InitializeComponent();
            ComWindow.Start(this);

            #region 时间秒表
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += ((a, b) =>
            {
                time.Content = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            });
            timer.IsEnabled = true;
            timer.Start();
            #endregion

            TextLogUtil.Info("启动成功");
            Task.Run(() =>
            {
                try
                {
                    var ini = new IniFile("Config/Config.ini");
                    var portName = ini.readKey("BoxConfig", "portName");
                    var baudRate = ini.readKey("BoxConfig", "baudRate");
                    var result = LockManager.GetInstance().Start(portName, baudRate.ToInt32());
                    if (!result.result)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            MessageDialog.ShowDialog(result.message);
                        });
                    }

                    #region 只启动一次
                    var common = BaiduFaceApiHelper.GetInstance().Start();
                    if (!common.result)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            MessageDialog.ShowDialog(common.message);
                        });
                    }
                    #endregion
                }
                catch (Exception e)
                {
                    TextLogUtil.Info(e.Message);
                }
            });

            #region 队列方式盘点
            Task.Run(() =>
            {
                var biz = new CommBiz();
                biz.GetAdminInfo();

                var ini = new IniFile("Config/Config.ini");

                var isNetWork = ini.readKey("InventoryConfig", "isNetWork").Trim();

                try
                {
                    var antbox = ini.readKey("InventoryConfig", "antbox").Trim();
                    var banlist = antbox.Split('#');
                    foreach (var item in banlist)
                    {
                        var arr = item.Split('_');
                        var boxs = arr[0].Trim().ToInt32();
                        var ant = arr[1].Split('+').ToList();
                        ClientAntList.Add(new AntBox() { BoxNum = boxs, AntList = ant });
                    }
                }
                catch (Exception e)
                {
                    MessageDialog.ShowDialog("天线箱门参数配置错误");
                }



                if (isNetWork == "1")
                {
                    var port = ini.readKey("InventoryConfig", "port");
                    var result2222 = RFIDHelper.GetInstance().ConnectNetWork(port.ToInt32());
                    if (!result2222.result)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            MessageDialog.ShowDialog(result2222.message);
                        });
                    }
                    var isOpenDouble = ini.readKey("InventoryConfig", "isOpenDouble").Trim();
                    if (isOpenDouble == "1")
                    {
                        var port2 = ini.readKey("InventoryConfig", "port2");
                        var result222211 = RFIDHelper.GetInstance().ConnectNetWork(port2.ToInt32());
                        if (!result222211.result)
                        {
                            Dispatcher.Invoke(() =>
                            {
                                MessageDialog.ShowDialog(result222211.message + "第二个读写器");
                            });
                        }
                    }
                }
                else
                {
                    var portName = ini.readKey("InventoryConfig", "portName");
                    var baudRate = ini.readKey("InventoryConfig", "baudRate").ToInt32();
                    var result = RFIDHelper.GetInstance().ConnectCOM(portName, baudRate);
                    if (!result.result)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            MessageDialog.ShowDialog(result.message);
                        });
                    }
                    var isOpenDouble = ini.readKey("InventoryConfig", "isOpenDouble").Trim();
                    if (isOpenDouble == "1")
                    {
                        var portNameDouble = ini.readKey("InventoryConfig", "portNameDouble");
                        var baudRateDouble = ini.readKey("InventoryConfig", "baudRateDouble").ToInt32();
                        var resultDouble = RFIDHelper2.GetInstance().ConnectCOM(portNameDouble, baudRateDouble);
                        if (!resultDouble.result)
                        {
                            Dispatcher.Invoke(() =>
                            {
                                MessageDialog.ShowDialog(resultDouble.message + "第二个读写器");
                            });
                        }
                    }
                }

                while (true)
                {
                    if (que.GetMessageCount() > 0)
                    {
                        var obj = que.Dequeue();
                        if (obj != null)
                        {
                            var temp = (QueueData)obj;

                            while (true)
                            {
                                var t = CheckBoxNum(temp);
                                if (t.Result == false)
                                {
                                    List<RFIDHelper.RFIDInfo> list = new List<RFIDHelper.RFIDInfo>();
                                    var fsss=ClientAntList.Where(p => p.BoxNum == temp.BoxNum).FirstOrDefault();
                                    foreach (var item in fsss.AntList)
                                    {
                                        var tttt = item.ToInt32();

                                        if (tttt <= 64)
                                        {
                                            var lists = RFIDHelper.GetInstance().StartOnceReadByAnt(tttt);
                                            list.AddRange(lists);
                                        }
                                        else
                                        {
                                            var tempnum = tttt - 64;
                                            var lists = RFIDHelper2.GetInstance().StartOnceReadByAnt(tempnum);
                                            list.AddRange(lists);
                                        }
                                    }

                                    biz.Deposit(temp.UserName, temp.BoxNum, list, temp.Mode);
                                    break;
                                }
                                else
                                {
                                    Thread.Sleep(1000);
                                }
                            }
                        }
                    }
                    else
                    {
                        Thread.Sleep(1000);
                    }
                }
            });
            #endregion

            if (Minute != null)
            {
                var tian = (Minute.Value / 60 / 24) + 1;
                if (tian <= 0)
                {
                    tian = 0;
                }else if (Minute == null || Minute <= 0)
                {
                    tian = 0;
                }
                auth.Content = "授权倒计时:" + tian + "天";
            }
        }
        public bool Auth()
        {
            if (Minute == null || Minute <= 0)
            {
                MessageDialog.ShowDialog("当前授权软件已过期，请联系厂家");
                return true;
            }
            return false;
        }


        //取CPU编号
        public String GetCpuID()
        {
            try
            {
                ManagementClass mc = new ManagementClass("Win32_Processor");
                ManagementObjectCollection moc = mc.GetInstances();
                String strCpuID = null;
                foreach (ManagementObject mo in moc)
                {
                    strCpuID = mo.Properties["ProcessorId"].Value.ToString();
                    break;
                }
                return strCpuID;
            }
            catch
            {
                return null;
            }
        }
        //取第⼀块硬盘编号
        public String GetHardDiskID()
        {
            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMedia");
                String strHardDiskID = null;
                foreach (ManagementObject mo in searcher.Get())
                {
                    strHardDiskID = mo["SerialNumber"].ToString().Trim();
                    break;
                }
                return strHardDiskID;
            }
            catch
            {
                return null;
            }
        }


        public Task<bool?> CheckBoxNum(QueueData temp)
        {
            TaskCompletionSource<bool?> tcs = new TaskCompletionSource<bool?>();
            //检测箱门是否关闭
            LockManager.GetInstance().CheckBox(temp.BoxNum, (a) =>
            {
                tcs.SetResult(a);
            });
            return tcs.Task;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
        private void Window_Closed(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                if (Auth())
                {
                    return;
                }

                Login login = new Login();
                login.ShowDialog();
            }
        }
        //站长开箱
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Auth())
            {
                return;
            }


            Password pwd=new Password();
            if (pwd.ShowDialog() != true)
            {
                return;
            }

            Start:

            SelectBox selbox = new SelectBox(1);
            if (selbox.ShowDialog() != true)
            {
                return;
            }
            var boxnum = selbox.SelectGoodsControl.BoxNum;

            var biz=new CommBiz();
            if (biz.IsLock(boxnum))
            {
                MessageDialog.ShowDialog("当前箱门已被锁定，请联系管理员解锁");
                return;
            }

            LockManager.GetInstance().OpenBox(Convert.ToInt32(boxnum));
            VoiceHelper.GetInstance().Start(boxnum + "号箱门已打开，操作完成后请随手关门");

            QueueData qdata=new QueueData();
            qdata.BoxNum = boxnum;
            qdata.Mode = "密码";
            qdata.UserName = "站长";
            que.Enqueue(qdata);

            SuccessTip2 tip =new SuccessTip2(boxnum);
            if (tip.ShowDialog() != true)
            {
                TempButton.Focus();
                return;
            }
            goto Start;
        }
        //单警装备
        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            if (Auth())
            {
                return;
            }


            SelectType sel=new SelectType();
            if (sel.ShowDialog() != true)
            {
                return;
            }
            UserInfo user = null;
            var type = sel.SelectTypes;
            if (type == 1)
            {
                FaceVer face=new FaceVer();
                if (face.ShowDialog() != true)
                {
                    return;
                }
                user = face.user;
            }
            else
            {
                CardVer face = new CardVer();
                if (face.ShowDialog() != true)
                {
                    return;
                }
                user = face.SelectUserInfo;
            }

            if (user.BoxNum == null)
            {
                MessageDialog.ShowDialog("当前用户没有绑定单警柜");
                return;
            }

            var boxnum = user.BoxNum.Value;

            var biz = new CommBiz();
            if (biz.IsLock(boxnum))
            {
                MessageDialog.ShowDialog("当前箱门已被锁定，请联系管理员解锁");
                return;
            }

            LockManager.GetInstance().OpenBox(Convert.ToInt32(boxnum));
            VoiceHelper.GetInstance().Start(boxnum + "号箱门已打开，操作完成后请随手关门");

            QueueData qdata = new QueueData();
            qdata.BoxNum = boxnum;
            qdata.Mode = type==1?"人脸":"刷卡";
            qdata.UserName = user.UserName;
            que.Enqueue(qdata);

            SuccessTip succ =new SuccessTip(boxnum);
            succ.ShowDialog();

            TempButton.Focus();

        }
        //公共装备
        private void Button_Click3(object sender, RoutedEventArgs e)
        {
            if (Auth())
            {
                return;
            }

            SelectType sel = new SelectType();
            if (sel.ShowDialog() != true)
            {
                return;
            }
            UserInfo user = null;
            var type = sel.SelectTypes;
            if (type == 1)
            {
                FaceVer face = new FaceVer();
                if (face.ShowDialog() != true)
                {
                    return;
                }
                user = face.user;
            }
            else
            {
                CardVer face = new CardVer();
                if (face.ShowDialog() != true)
                {
                    return;
                }
                user = face.SelectUserInfo;
            }

            if (user.IsPublicBox != 1)
            {
                MessageDialog.ShowDialog("当前用户没有公共柜权限");
                return;
            }

            Start:
            SelectBox selbox = new SelectBox(0);
            if (selbox.ShowDialog() != true)
            {
                return;
            }

            var boxnum = selbox.SelectGoodsControl.BoxNum;

            var biz = new CommBiz();
            if (biz.IsLock(boxnum))
            {
                MessageDialog.ShowDialog("当前箱门已被锁定，请联系管理员解锁");
                return;
            }

            LockManager.GetInstance().OpenBox(Convert.ToInt32(boxnum));
            VoiceHelper.GetInstance().Start(boxnum + "号箱门已打开，操作完成后请随手关门");

            QueueData qdata = new QueueData();
            qdata.BoxNum = boxnum;
            qdata.Mode = type == 1 ? "人脸" : "刷卡";
            qdata.UserName = user.UserName;
            que.Enqueue(qdata);

            SuccessTip2 tip = new SuccessTip2(boxnum);
            if (tip.ShowDialog() != true)
            {
                TempButton.Focus();
                return;
            }
            goto Start;

        }
        //装备查询
        private void Button_Click4(object sender, RoutedEventArgs e)
        {
            if (Auth())
            {
                return;
            }


            LogEdit log=new LogEdit();
            log.ShowDialog();

            TempButton.Focus();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {

        }
    }
}
