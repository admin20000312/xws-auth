﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DJ.UserControls
{
    /// <summary>
    /// ClientBox.xaml 的交互逻辑
    /// </summary>
    public partial class ClientBox : UserControl
    {
        public int BoxNum = 0;

        public bool IsCheck = false;
        public ClientBox()
        {
            InitializeComponent();
        }
    }
}
