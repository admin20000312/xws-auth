﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DJ.ZEF.ThirdParty;
using NSJL.Framework.Utils;

namespace DJ.Third
{
    /// <summary>
    /// CardNo.xaml 的交互逻辑
    /// </summary>
    public partial class CardNo : Window
    {
        public CardNo()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();

            try
            {
                var ini = new IniFile("Config/Config.ini");
                var portName = ini.readKey("CardConfig", "portName");
                var baudRate = ini.readKey("CardConfig", "baudRate").ToInt32();

                CardHelper.GetInstance().Start(portName, baudRate, (a) =>
                {
                    Dispatcher.Invoke(() =>
                    {
                        TextBox1.Text = a;
                    });
                    Thread.Sleep(1000);
                    Dispatcher.Invoke(() =>
                    {
                        this.DialogResult = true;
                        this.Close();
                    });
                });
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            CardHelper.GetInstance().Stop();
        }
    }
}
