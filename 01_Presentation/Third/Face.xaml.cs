﻿using DJ.ZEF.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DJ.Dialog;
using System.Drawing;
using System.Windows.Interop;
using NSJL.Framework.Utils;
using System.Windows.Threading;

namespace DJ.Third
{
    /// <summary>
    /// Face.xaml 的交互逻辑
    /// </summary>
    public partial class Face : Window
    {
        public Face()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += ((a, b) =>
            {
                var miao = Label1.Tag.ToInt32() - 1;
                Label1.Tag = miao;
                if (miao <= 0)
                {
                    this.Close();
                    return;
                }
                Label1.Content = "倒计时：" + Label1.Tag + "秒";
            });
            timer.IsEnabled = true;
            timer.Start();


            var result=VideoHelper.GetInstance().Start(ImageWith, Success);
            if (!result.result)
            {
                MessageDialog.ShowDialog(result.message);
                this.Close();
            }
        }

        public void ImageWith(Bitmap map)
        {
            try
            {
                Dispatcher.Invoke(() =>
                {
                    try
                    {
                        Image1.Source = ConvertToBitmapSource(map);
                    }
                    catch (Exception e)
                    {
                    }
                });
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }
        }
        public void Success(byte[] imageBytes)
        {
            try
            {
                Facebytes = (byte[])imageBytes.Clone();
                Dispatcher.Invoke(() =>
                {
                    try
                    {
                        this.DialogResult = true;
                        this.Close();
                    }
                    catch (Exception e)
                    {
                    }
                });
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);   
            }
        }
        public byte[] Facebytes = null;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        public static System.Windows.Media.Imaging.BitmapSource ConvertToBitmapSource(Bitmap btmap)
        {
            var ptr = btmap.GetHbitmap();
            var temp = Imaging.CreateBitmapSourceFromHBitmap(ptr, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            DeleteObject(ptr);
            return temp;
        }
        [System.Runtime.InteropServices.DllImport("gdi32.dll", SetLastError = true)]
        private static extern bool DeleteObject(IntPtr hObject);

        private void Window_Closed(object sender, EventArgs e)
        {
            VideoHelper.GetInstance().Stop();
        }
    }
}
