﻿using DJ.ZEF.ThirdParty;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Third
{
    /// <summary>
    /// Finger.xaml 的交互逻辑
    /// </summary>
    public partial class Finger : Window
    {
        public Finger()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();

            try
            {
                var result = FingerHelper.GetInstance().Start((a, b, c) =>
                {
                    Features = b;
                    Dispatcher.Invoke(() =>
                    {
                        Image1.Source = SelfUtil.ConvertToBitmapSource(a);
                    });
                    Thread.Sleep(1000);
                    Dispatcher.Invoke(() =>
                    {
                        this.DialogResult = true;
                        this.Close();
                    });
                }, null);
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }
        }

        public string Features = null;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void Window_Closed(object sender, EventArgs e)
        {
            FingerHelper.GetInstance().Stop();
        }
    }
}
