﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using NSJL.Framework.Utils;

namespace DJ
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(identity);
            //判断当前登录用户是否为管理员
            if (principal.IsInRole(WindowsBuiltInRole.Administrator))
            {
                //如果是管理员，则直接运行
                //Run(new Client());
            }
            else
            {
                //创建启动对象
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.UseShellExecute = true;
                startInfo.WorkingDirectory = Environment.CurrentDirectory;
                startInfo.FileName = Assembly.GetExecutingAssembly().Location;
                //设置启动动作,确保以管理员身份运行
                startInfo.Verb = "runas";
                try
                {
#if DEBUG

#else
                    SelfUtil.SetAutoBoot("DJ",true);
                    System.Diagnostics.Process.Start(startInfo);
                    Application.Current.Shutdown();
#endif
                }
                catch
                {
                    return;
                }
            }
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            Task.Run(() =>
            {
                Thread.Sleep(1000);
                string MName = System.Diagnostics.Process.GetCurrentProcess().MainModule.ModuleName;
                string PName = System.IO.Path.GetFileNameWithoutExtension(MName);
                System.Diagnostics.Process[] myProcess = System.Diagnostics.Process.GetProcessesByName(PName);

                if (myProcess.Length > 1)
                {
                    Dispatcher.Invoke(() =>
                    {
                        MessageBox.Show("本程序一次只能运行一个实例！", "提示");
                        Application.Current.Shutdown();
                        Environment.Exit(0);
                        return;
                    });
                }
            });

            DispatcherUnhandledException += App_DispatcherUnhandledException;
            base.OnStartup(e);
        }

        void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            TextLogUtil.Info("程序异常：" + e.Exception.Source + "@@" + e.Exception.Message);
        }
    }
}
