﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DJ.Clients;
using DJ.Dialog;
using DJ.Pages;
using DJ.ZEF.ThirdParty;
using NSJL.Framework.Utils;

namespace DJ
{
    /// <summary>
    /// Main.xaml 的交互逻辑
    /// </summary>
    public partial class Main : Window
    {
        private static readonly object locks = new object();
        private static Main manage;
        public static Main GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new Main());
            }
        }
        public Main()
        {
            //WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
            ComWindow.Start(this);
            //登录修改用户名
            Label1.Content = UserName;
            //初始化page
            Button1_Click(Button1, null);
            page5.action = (()=>
            {
                //page1.Init();
                //page8.Init();
            });

            double y1 = SystemParameters.PrimaryScreenHeight;
            TextLogUtil.Info("屏幕高度："+y1);
            if (y1 >= 1024)
            {
                Temp1.Height = 781;
            }
        }

        public static string UserName = null;
        //返回主页
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Hide();
            //this.Close();
        }
        //退出系统
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }


        private BoxPage page1=new BoxPage();
        private UserPage page2 = new UserPage();
        private AccessPage page3 = new AccessPage();
        private GoodsPage page4 = new GoodsPage();
        private SetBoxPage page5 = new SetBoxPage();
        private EditPwdPage page6 = new EditPwdPage();

        private RolePage page7 = new RolePage();
        private GoodsPage page8 = new GoodsPage();

        #region 导航按钮
        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            SetBorder(sender);
            Frame1.Content = page1;
        }
        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            SetBorder(sender);
            Frame1.Content = page2;
        }
        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            SetBorder(sender);
            Frame1.Content = page3;
        }
        private void Button4_Click(object sender, RoutedEventArgs e)
        {
            SetBorder(sender);
            Frame1.Content = page4;
        }
        private void Button5_Click(object sender, RoutedEventArgs e)
        {
            SetBorder(sender);
            Frame1.Content = page5;
        }
        private void Button6_Click(object sender, RoutedEventArgs e)
        {
            SetBorder(sender);
            Frame1.Content = page6;
        }
        public void SetBorder(object sender)
        {
            Button1.Foreground = (Brush)new BrushConverter().ConvertFromString("#000000");
            Button1.BorderThickness = new Thickness(1, 1, 1, 1);

            Button2.Foreground = (Brush)new BrushConverter().ConvertFromString("#000000");
            Button2.BorderThickness = new Thickness(1, 1, 1, 1);

            Button3.Foreground = (Brush)new BrushConverter().ConvertFromString("#000000");
            Button3.BorderThickness = new Thickness(1, 1, 1, 1);

            Button4.Foreground = (Brush)new BrushConverter().ConvertFromString("#000000");
            Button4.BorderThickness = new Thickness(1, 1, 1, 1);

            Button5.Foreground = (Brush)new BrushConverter().ConvertFromString("#000000");
            Button5.BorderThickness = new Thickness(1, 1, 1, 1);

            Button6.Foreground = (Brush)new BrushConverter().ConvertFromString("#000000");
            Button6.BorderThickness = new Thickness(1, 1, 1, 1);

            //Button7.Foreground = (Brush)new BrushConverter().ConvertFromString("#000000");
            //Button7.BorderThickness = new Thickness(1, 1, 1, 1);

            //Button8.Foreground = (Brush)new BrushConverter().ConvertFromString("#000000");
            //Button8.BorderThickness = new Thickness(1, 1, 1, 1);

            ((Button)sender).Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
            ((Button)sender).BorderThickness = new Thickness();
        }

        #endregion
        //日志记录
        private void Button7_Click(object sender, RoutedEventArgs e)
        {
            SetBorder(sender);
            Frame1.Content = page7;
        }
        //配置管理
        private void Button8_Click(object sender, RoutedEventArgs e)
        {
            SetBorder(sender);
            Frame1.Content = page8;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            manage = null;
        }
    }
}
