﻿using DJ.ZEF.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DJ.Clients
{
    /// <summary>
    /// FailTip.xaml 的交互逻辑
    /// </summary>
    public partial class FailTip : Window
    {
        DispatcherTimer timer = new DispatcherTimer();
        public FailTip(int boxNum)
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            WindowState = WindowState.Maximized;

            BoxNum = boxNum;
        }
        public int BoxNum { get; set; }

        //强制关门
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            timer.Stop();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += ((a, b) =>
            {
                LockManager.GetInstance().CheckBox(BoxNum, (isclose) =>
                {
                    if (isclose == false)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            DialogResult = true;
                            Close();
                        });
                    }
                });
            });
            timer.IsEnabled = true;
            timer.Start();
        }
    }
}
