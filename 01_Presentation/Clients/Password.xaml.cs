﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DJ.Dialog;
using NSJL.Biz.Background.Client;

namespace DJ.Clients
{
    /// <summary>
    /// Password.xaml 的交互逻辑
    /// </summary>
    public partial class Password : Window
    {
        public Password()
        {
            InitializeComponent();
            ComWindow.Start(this, Label1);



        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click111(object sender, RoutedEventArgs e)
        {
            var pwd = PasswordBox1.Password;
            var biz = new CommBiz();
            var result=biz.VerZhanz(pwd);
            if (!result.result)
            {
                MessageDialog.ShowDialog(result.message);
                return;
            }

            DialogResult = true;
            this.Close();
        }

        private void PasswordBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }
    }
}
