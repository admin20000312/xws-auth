﻿using DJ.UserControls;
using NSJL.Biz.Background.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DJ.Dialog;
using NSJL.DAL.DataModel.Entities;
using NSJL.Framework.Utils;

namespace DJ.Clients
{
    public partial class SelectBox : Window
    {
        public SelectBox(int Isall = 1)
        {
            InitializeComponent();
            ComWindow.Start(this, Label1, true);

            Init(Isall);
        }
        public void Init(int Isall)
        {
            WrapPanel1.Children.Clear();
            var biz = new CommBiz();
            var list = biz.GetBoxListWithType(Isall);
            var glist = biz.GetGoodsTypeList2("在库");
            foreach (var item in list)
            {
                BoxControl2 btn = new BoxControl2();
                btn.Width = 154;
                btn.Height = 125;
                btn.Margin = new Thickness(5, 5, 5, 5);
                btn.Label1.Content = item.BoxNum;
                btn.BoxNum = item.BoxNum;

                var group = glist.Where(p => p.BoxNum == item.BoxNum).GroupBy(p => p.Name).OrderByDescending(p => p.Count()).ToList();
                if (group.Count == 0)
                {
                    btn.Label2.Text = "空闲";
                }
                else
                {
                    var tempstr = "";
                    foreach (var temp in group)
                    {
                        tempstr += temp.Key + "x" + temp.Count() + ",";
                    }
                    tempstr = tempstr.Substring(0, tempstr.Length - 1);
                    if (tempstr.Length > 40)
                    {
                        btn.Label2.Text = tempstr.Substring(0, 40) + "...";
                    }
                    else
                    {
                        btn.Label2.Text = tempstr;
                    }
                }

                if (item.IsSingle == 1)
                {
                    btn.Label4.Content = "单警柜";
                }
                else
                {
                    btn.Label4.Content = "公共柜";
                }
                btn.MouseDown += MouseButtonEventHandler;
                WrapPanel1.Children.Add(btn);
            }
        }
        public BoxControl2 SelectGoodsControl { get; set; }
        public void MouseButtonEventHandler(object sender, RoutedEventArgs e)
        {
            var u = (BoxControl2)sender;
            if (u.IsCheck)
            {
                if (SelectGoodsControl == u)
                {
                    return;
                }

                u.IsCheck = false;
                var filepath = "pack://application:,,,/Image/a未选中.png";
                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                u.Label2.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                u.Label4.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
            }
            else
            {
                u.IsCheck = true;
                var filepath = "pack://application:,,,/Image/a选中.png";
                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                u.Label2.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                u.Label4.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
            }
            if (SelectGoodsControl != null)
            {
                SelectGoodsControl.IsCheck = false;
                var filepath = "pack://application:,,,/Image/a未选中.png";
                SelectGoodsControl.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                SelectGoodsControl.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                SelectGoodsControl.Label2.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                SelectGoodsControl.Label4.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
            }
            SelectGoodsControl = u;
        }


        
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            if (SelectGoodsControl == null)
            {
                MessageDialog.ShowDialog("请选择箱门");
                return;
            }

            DialogResult = true;
            Close();
        }



        private void SCManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

    }
}
