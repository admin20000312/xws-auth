﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients
{
    /// <summary>
    /// SuccessTip2.xaml 的交互逻辑
    /// </summary>
    public partial class SuccessTip2 : Window
    {
        public SuccessTip2(int boxNum)
        {
            InitializeComponent();
            ComWindow.Start(this,Label1,true);

            tip.Text = boxNum + "号箱门已打开，操作完成后请随手关好箱门!";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Button_Click54546(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
