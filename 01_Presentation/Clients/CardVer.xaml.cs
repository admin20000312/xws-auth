﻿using DJ.Dialog;
using DJ.ZEF.ThirdParty;
using NSJL.Biz.Background.Client;
using NSJL.DAL.DataModel.Entities;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients
{
    /// <summary>
    /// CardVer.xaml 的交互逻辑
    /// </summary>
    public partial class CardVer : Window
    {
        public CardVer()
        {
            InitializeComponent();
            ComWindow.Start(this,Label1);


            var ini = new IniFile("Config/Config.ini");
            var portName = ini.readKey("CardConfig", "portName");
            var baudRate = ini.readKey("CardConfig", "baudRate").ToInt32();


            CardHelper.GetInstance().Start(portName, baudRate, (a) =>
            {
                Dispatcher.Invoke(() =>
                {
                    var biz = new CommBiz();
                    var result = biz.GetUserInfoWithCode(a);
                    if (result == null)
                    {
                        MessageDialog.ShowDialog("卡号对应用户不存在");
                        return;
                    }

                    SelectUserInfo = result;
                    this.DialogResult = true;
                    this.Close();
                });
            });
        }
        public UserInfo SelectUserInfo { get; set; }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            CardHelper.GetInstance().Stop();
        }
    }
}
