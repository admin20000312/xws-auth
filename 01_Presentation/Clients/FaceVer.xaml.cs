﻿using DJ.Dialog;
using DJ.ZEF.ThirdParty;
using NSJL.Biz.Background.Client;
using NSJL.DAL.DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DJ.Clients
{
    /// <summary>
    /// FaceVer.xaml 的交互逻辑
    /// </summary>
    public partial class FaceVer : Window
    {
        public FaceVer()
        {
            InitializeComponent();
            ComWindow.Start(this,Label1);
        }
       
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GC.Collect();
            var result = VideoHelper.GetInstance().Start(ShowImage, FaceResult);
            if (!result.result)
            {
                Task.Run(() =>
                {
                    Dispatcher.Invoke(() =>
                    {
                        MessageDialog.ShowDialog(result.message);
                        this.Close();
                    });
                });
            }
        }
        public void ShowImage(Bitmap a)
        {
            Dispatcher.Invoke(() =>
            {
                try
                {
                    Image1.Source = ConvertToBitmapSource(a);
                }
                catch (Exception e)
                {
                }
            });
        }
        public void FaceResult(byte[] b)
        {
            var call = BaiduFaceApiHelper.GetInstance().Contrast(b);
            Dispatcher.Invoke(() =>
            {
                try
                {
                    if (call.result)
                    {
                        var biz = new CommBiz();
                        var res = biz.GetUserInfoWithFaceId(call.data);
                        if (res.result)
                        {
                            user = res.data;
                            this.DialogResult = true;
                            this.Close();
                        }
                        else
                        {
                            VoiceHelper.GetInstance().Start("用户不存在");
                            MessageDialog.ShowDialog(res.message);
                            this.Close();
                        }
                    }
                    else
                    {
                        MessageDialog.ShowDialog(call.message);
                        this.Close();
                    }
                }
                catch (Exception e)
                {
                }
            });
        }
        public UserInfo user { get; set; }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            VideoHelper.GetInstance().Stop();
        }

        public static System.Windows.Media.Imaging.BitmapSource ConvertToBitmapSource(Bitmap btmap)
        {
            var ptr = btmap.GetHbitmap();
            var temp = Imaging.CreateBitmapSourceFromHBitmap(ptr, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            DeleteObject(ptr);
            return temp;
        }
        [System.Runtime.InteropServices.DllImport("gdi32.dll", SetLastError = true)]
        private static extern bool DeleteObject(IntPtr hObject);

        
    }
}
