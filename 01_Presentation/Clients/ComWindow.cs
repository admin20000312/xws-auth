﻿using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace DJ.Clients
{
    public class ComWindow
    {
        public static void Start(Window window ,Label Label1=null,bool flag=false)
        {
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;

//#if DEBUG
//            window.WindowState = WindowState.Normal;
//#else
            window.WindowState = WindowState.Maximized;
//#endif



            if (Label1 != null)
            {
                DispatcherTimer timer = new DispatcherTimer();
                timer.Interval = new TimeSpan(0, 0, 1);
                timer.Tick += ((a, b) =>
                {
                    var miao = Label1.Tag.ToInt32() - 1;
                    Label1.Tag = miao;
                    if (miao <= 0)
                    {
                        ((DispatcherTimer)a).Stop();
                        window.Close();
                        return;
                    }
                    if (flag)
                    {
                        Label1.Content =Label1.Tag + "S";
                    }
                    else
                    {
                        Label1.Content = "倒计时："+Label1.Tag + "秒";
                    }
                });
                timer.IsEnabled = true;
                timer.Start();
            }
        }


        public static void Focus(object sender)
        {
            if (sender.GetType().Name == "PasswordBox")
            {
                var pwd = (PasswordBox)sender;
                if (!string.IsNullOrWhiteSpace(pwd.Password))
                {
                    pwd.GetType().GetMethod("Select", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).Invoke(pwd, new object[] { pwd.Password.Length, 0 });
                }
                pwd.Focus();
            }
            else
            {
                var txt = (TextBox)sender;
                txt.SelectionStart = (txt.Text ?? "").Length;
                txt.Focus();
            }
            SelfUtil.Open();
        }
    }
}
