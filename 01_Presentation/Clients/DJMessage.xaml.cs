﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients
{
    /// <summary>
    /// DJMessage.xaml 的交互逻辑
    /// </summary>
    public partial class DJMessage : Window
    {
        public DJMessage()
        {
            InitializeComponent();
            ComWindow.Start(this,Label1);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
