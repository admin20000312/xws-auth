﻿using DJ.ZEF.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using DJ.Dialog;
using NSJL.Framework.Utils;

namespace DJ.Clients
{
    /// <summary>
    /// SuccessTip.xaml 的交互逻辑
    /// </summary>
    public partial class SuccessTip : Window
    {
        public SuccessTip(int boxNum)
        {
            InitializeComponent();
            ComWindow.Start(this,Label1,true);

            tip.Text = boxNum + "号箱门已打开，操作完成后请随手关好箱门!";

            BoxNum = boxNum;
        }
        public int BoxNum { get; set; }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void Window_Closed(object sender, EventArgs e)
        {
        }

        private void Button_Click54546(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
