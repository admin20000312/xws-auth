﻿using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DJ.Dialog;
using DJ.ZEF.ThirdParty;
using NSJL.Biz.Background.Client;

namespace DJ.Clients
{
    /// <summary>
    /// Inventory.xaml 的交互逻辑
    /// </summary>
    public partial class Inventory : Window
    {
        public List<RFIDHelper.RFIDInfo> list=new List<RFIDHelper.RFIDInfo>();
        public Inventory(int boxnum)
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            WindowState = WindowState.Maximized;

            BoxNum = boxnum;
        }
        public int BoxNum { get; set; }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var ini = new IniFile("Config/Config.ini");
            var portName = ini.readKey("InventoryConfig", "portName");
            var baudRate = ini.readKey("InventoryConfig", "baudRate");
            var result = RFIDHelper.GetInstance().ConnectCOM(portName, baudRate.ToInt32());
            if (!result.result)
            {
                MessageDialog.ShowDialog(result.message);
                Close();
                return;
            }


            Task.Run(() =>
            {
                list = RFIDHelper.GetInstance().StartOnceReadByAnt(BoxNum);
                System.Windows.Application.Current.Dispatcher.Invoke(() =>
                {
                    this.Close();
                });
            });

        }
        private void Window_Closed(object sender, EventArgs e)
        {
            RFIDHelper.GetInstance().Close();
        }

        
    }
}
