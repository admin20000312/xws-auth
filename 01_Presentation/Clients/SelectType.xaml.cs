﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients
{
    /// <summary>
    /// SelectType.xaml 的交互逻辑
    /// </summary>
    public partial class SelectType : Window
    {
        public SelectType()
        {
            InitializeComponent();
            ComWindow.Start(this,Label1);
        }
        public int SelectTypes { get; set; }
        //人脸识别
        private void Button_Click111(object sender, RoutedEventArgs e)
        {
            SelectTypes = 1;
            DialogResult = true;
            Close();
        }
        //刷卡
        private void Button_Click_222(object sender, RoutedEventArgs e)
        {
            SelectTypes = 2;
            DialogResult = true;
            Close();
        }
        //返回
        private void Button_Click54546(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
