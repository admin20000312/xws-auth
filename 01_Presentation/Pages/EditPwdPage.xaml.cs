﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DJ.Clients;
using DJ.Dialog;
using NSJL.Biz.Background.Client;
using NSJL.Framework.Utils;

namespace DJ.Pages
{
    /// <summary>
    /// EditPwdPage.xaml 的交互逻辑
    /// </summary>
    public partial class EditPwdPage : Page
    {
        public EditPwdPage()
        {
            InitializeComponent();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(PasswordBox1.Password) || string.IsNullOrWhiteSpace(PasswordBox2.Password) ||
                string.IsNullOrWhiteSpace(PasswordBox3.Password))
            {
                MessageDialog.ShowDialog("密码不能为空");
                return;
            }
            if (PasswordBox2.Password != PasswordBox3.Password)
            {
                MessageDialog.ShowDialog("二次输入密码不相同");
                return;
            }

            var biz = new CommBiz();
            var result = biz.EditPassword(Main.UserName, PasswordBox1.Password, PasswordBox3.Password);
            if (result.result)
            {
                PasswordBox1.Password = "";
                PasswordBox2.Password = "";
                PasswordBox3.Password = "";
                MessageDialog.ShowDialog("修改成功");
            }
            else
            {
                MessageDialog.ShowDialog(result.message);
            }
        }

        private void PasswordBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }

        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(PasswordBox2_Copy.Password) ||
                string.IsNullOrWhiteSpace(PasswordBox3_Copy.Password))
            {
                MessageDialog.ShowDialog("密码不能为空");
                return;
            }
            if (PasswordBox2_Copy.Password != PasswordBox3_Copy.Password)
            {
                MessageDialog.ShowDialog("二次输入密码不相同");
                return;
            }

            var biz = new CommBiz();
            var result = biz.EditPassword2(PasswordBox3_Copy.Password);
            if (result.result)
            {
                PasswordBox2_Copy.Password = "";
                PasswordBox3_Copy.Password = "";
                MessageDialog.ShowDialog("修改成功");
            }
            else
            {
                MessageDialog.ShowDialog(result.message);
            }
        }
    }
}
