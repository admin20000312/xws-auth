﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DJ.Clients;
using DJ.Dialog;
using NSJL.Biz.Background.Client;
using NSJL.DAL.DataModel.Entities;
using NSJL.DomainModel.Background.Client;
using NSJL.Framework.Utils;

namespace DJ.Pages
{
    /// <summary>
    /// UserPage.xaml 的交互逻辑
    /// </summary>
    public partial class UserPage : Page
    {
        public UserPage()
        {
            InitializeComponent();
            
        }
        private void Page_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Init(null);
        }
        public CommBiz biz=new CommBiz();
        public void Init(string name)
        {
            var list = biz.GetUserList(name);
            DataGrid1.ItemsSource = null;
            DataGrid1.ItemsSource = list;
        }
        //查询
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var name = TextBox1.Text;
            Init(name);
        }
        //显示全部
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            Init(null);
        }
        //添加用户
        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            UserEdit user = new UserEdit();
            var result=user.ShowDialog();
            if (result != null && result.Value == true)
            {
                var username = user.UserName.Text;
                var code = user.code.Text;
                var faceid = user.faceId;
                string facebase64 = user.faceBase64;
                var ispulic=user.ComboBox1.Text=="是"?1:0;

                int? boxnum = null;
                if (user.ComboBox1_Copy.Text != "不绑定")
                {
                    boxnum= Convert.ToInt32(user.ComboBox1_Copy.Text);
                }

                #region 压缩图片代码  可直接注释
                try
                {
                    if (facebase64 != null)
                    {
                        var temp = Convert.FromBase64String(facebase64);
                        var path = SelfUtil.GetPicThumbnail(temp);
                        if (path != null && File.Exists(path))
                        {
                            facebase64 = Convert.ToBase64String(File.ReadAllBytes(path));
                            File.Delete(path);
                        }
                    }
                }
                catch (Exception exception)
                {
                }
                #endregion

                var call=biz.EditUserInfo(null, username, code, faceid, facebase64, ispulic, boxnum);
                if (!call.result)
                {
                    MessageDialog.ShowDialog(call.message);
                    return;
                }
                MessageDialog.ShowDialog("添加成功");

                Init(null);
            }

        }
        //编辑
        private void Button_Click4(object sender, RoutedEventArgs e)
        {
            UserInfo info = (UserInfo)DataGrid1.SelectedItem;
            if (info == null)
            {
                MessageDialog.ShowDialog("未选中用户行");
                return;
            }
            UserEdit user = new UserEdit();
            user.UserName.Text = info.UserName;
            user.code.Text = info.CardNum;
            if (info.IsPublicBox == 1)
            {
                user.ComboBox1.SelectedIndex = 1;
            }
            if (info.BoxNum != null)
            {
                var itemss = new ComboBoxItem();
                itemss.Content = info.BoxNum;
                user.ComboBox1_Copy.Items.Add(itemss);
                user.ComboBox1_Copy.SelectedItem = itemss;
            }

            user.faceId = info.FaceId;
            if (string.IsNullOrWhiteSpace(info.HeadPic))
            {
                var filepath = "pack://application:,,,/Image/组 3@2x.png";
                user.headpic.Source = new BitmapImage(new Uri(filepath));
            }
            else
            {
                var base64 = Convert.FromBase64String(info.HeadPic);
                user.headpic.Source = GetImageWithByte(base64);
            }
            user.ShowDialog();
            if (user.DialogResult != null && user.DialogResult == true)
            {
                var username = user.UserName.Text;
                var code = user.code.Text;
                var faceid = user.faceId;
                string facebase64 = user.faceBase64;
                var ispulic = user.ComboBox1.Text == "是" ? 1 : 0;

                int? boxnum = null;
                if (user.ComboBox1_Copy.Text != "不绑定")
                {
                    boxnum = Convert.ToInt32(user.ComboBox1_Copy.Text);
                }
                #region 压缩图片代码  可直接注释
                try
                {
                    if (facebase64 != null)
                    {
                        var temp = Convert.FromBase64String(facebase64);
                        var path = SelfUtil.GetPicThumbnail(temp);
                        if (path != null && File.Exists(path))
                        {
                            facebase64 = Convert.ToBase64String(File.ReadAllBytes(path));
                            File.Delete(path);
                        }
                    }
                }
                catch (Exception exception)
                {
                }
                #endregion

                var call=biz.EditUserInfo(info.Id, username, code, faceid, facebase64, ispulic, boxnum);
                if (!call.result)
                {
                    MessageDialog.ShowDialog(call.message);
                    return;
                }
                MessageDialog.ShowDialog("修改成功");

                Init(null);
            }
        }
        //删除
        private void Button_Click5(object sender, RoutedEventArgs e)
        {
            UserInfo info = (UserInfo)DataGrid1.SelectedItem;
            if (info == null)
            {
                MessageDialog.ShowDialog("未选中用户行");
                return;
            }

            UserDel user = new UserDel();
            user.name.Content = info.UserName;
            //user.mobile.Content = info.UserCode;
            //user.jvse.Content = info.RoleId;

            if (string.IsNullOrWhiteSpace(info.HeadPic))
            {
                var filepath = "pack://application:,,,/Image/组 3@2x.png";
                user.headpic.Source = new BitmapImage(new Uri(filepath));
            }
            else
            {
                var base64 = Convert.FromBase64String(info.HeadPic);
                user.headpic.Source = GetImageWithByte(base64);
            }
            user.ShowDialog();
            if (user.DialogResult!=null && user.DialogResult==true)
            {
                //需要删除用户
                biz.DelUserInfo(info.Id);
                MessageDialog.ShowDialog("删除成功");
                Init(null);

            }
        }

        private void DataGrid1_MouseUp(object sender, MouseButtonEventArgs e)
        {
        }
        public BitmapImage GetImageWithByte(byte [] bytes)
        {
            System.IO.MemoryStream ms = new System.IO.MemoryStream(bytes);
            ms.Seek(0, System.IO.SeekOrigin.Begin);
            BitmapImage newBitmapImage = new BitmapImage();
            newBitmapImage.BeginInit();
            newBitmapImage.StreamSource = ms;
            newBitmapImage.EndInit();
            return newBitmapImage;
        }
        private void TextBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }
    }
}
