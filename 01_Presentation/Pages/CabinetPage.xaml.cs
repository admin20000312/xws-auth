﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DJ.Dialog;
using NSJL.Biz.Background.Client;
using NSJL.DAL.DataModel.Entities;

namespace DJ.Pages
{
    /// <summary>
    /// CabinetPage.xaml 的交互逻辑
    /// </summary>
    public partial class CabinetPage : Page
    {
        public CabinetPage()
        {
            InitializeComponent();
        }

        //删除
        private void Button_Click5(object sender, RoutedEventArgs e)
        {
            CabinetInfo info = (CabinetInfo)DataGrid1.SelectedItem;
            if (info == null)
            {
                MessageDialog.ShowDialog("未选中用户行");
                return;
            }

            CabinetDel del=new CabinetDel();
            del.name.Content = info.Name;
            var result=del.ShowDialog();
            if (result == true)
            {
                var biz = new CommBiz();
                var list = biz.GetCabinetList();
                DataGrid1.ItemsSource = null;
                DataGrid1.ItemsSource = list;
            }
        }
        //添加柜号
        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            CabinetEdit del = new CabinetEdit();
            var result = del.ShowDialog();
            if (result == true)
            {
                var biz = new CommBiz();
                var list = biz.GetCabinetList();
                DataGrid1.ItemsSource = null;
                DataGrid1.ItemsSource = list;
            }
        }

        private void Page_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var biz = new CommBiz();
            var list = biz.GetCabinetList();

            DataGrid1.ItemsSource = list;
        }
    }
}
