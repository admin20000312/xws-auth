﻿using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DJ.ZEF.ThirdParty;
using NSJL.Biz.Background.Client;
using DJ.Dialog;

namespace DJ.Pages
{
    /// <summary>
    /// SetBoxPage.xaml 的交互逻辑
    /// </summary>
    public partial class SetBoxPage : Page
    {
        public Action action;
        public SetBoxPage()
        {
            InitializeComponent();

            var p0 = ini.readKey("BoxConfig", "cabinetName");
            var p1 = ini.readKey("BoxConfig", "count");
            var p2 = ini.readKey("BoxConfig", "portName");
            var p3 = ini.readKey("BoxConfig", "baudRate");
            TextBox1.Text = p1;
            TextBox1_Copy.Text = p0;
            foreach (ComboBoxItem item in ComboBox1.Items)
            {
                if ((string)item.Content == p2)
                {
                    item.IsSelected = true;
                }
                else
                {
                    item.IsSelected = false;
                }
            }
            foreach (ComboBoxItem item in ComboBox2.Items)
            {
                if ((string)item.Content == p3)
                {
                    item.IsSelected = true;

                }
                else
                {
                    item.IsSelected = false;
                }
            }
            //ComboBox1.SelectedValue = p2;
            //ComboBox2.SelectedValue = p3;
        }
        public IniFile ini = new IniFile("Config/Config.ini");
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var p0 = TextBox1_Copy.Text;
                var p1 = TextBox1.Text.ToInt32();
                var p2 = ComboBox1.SelectionBoxItem.ToString();
                var p3 = ComboBox2.SelectionBoxItem.ToString();
                ini.writeKey("BoxConfig", "count", p1.ToString());
                ini.writeKey("BoxConfig", "portName", p2);
                ini.writeKey("BoxConfig", "baudRate", p3);
                ini.writeKey("BoxConfig", "cabinetName",p0);

                //初始化数据库
                var biz = new CommBiz();
                //var isca=biz.IsCabinet(p0);
                //if (!isca)
                //{
                //    MessageDialog.ShowDialog("柜号名不存在，请先添加柜号");
                //    return;
                //}

                biz.InitBoxInfo(p1, p0);

                LockManager.GetInstance().Close();
                LockManager.GetInstance().Start(p2, p3.ToInt32());

                MessageDialog.ShowDialog("操作成功");

                action?.Invoke();
            }
            catch (Exception ex)
            {
                MessageDialog.ShowDialog(ex.Message);
            }
        }

        private void TextBox1_Copy_GotFocus(object sender, RoutedEventArgs e)
        {
            SelfUtil.Open();
        }


    }
}
