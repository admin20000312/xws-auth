﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DJ.Dialog;
using DJ.UserControls;
using DJ.ZEF.ThirdParty;
using NSJL.Biz.Background.Client;

namespace DJ.Pages
{
    /// <summary>
    /// BoxPage.xaml 的交互逻辑
    /// </summary>
    public partial class BoxPage : Page
    {
        public BoxPage()
        {
            InitializeComponent();
            Init();
        }
        public void Init()
        {
            WrapPanel1.Children.Clear();
            var biz = new CommBiz();
            var list = biz.GetBoxList();
            var glist = biz.GetUserList(null);
            foreach (var item in list)
            {
                BoxControl btn = new BoxControl();
                btn.Width = 154;
                btn.Height = 125;
                btn.Margin = new Thickness(5, 5, 5, 5);
                btn.Label1.Content = item.BoxNum;
                btn.BoxNum = item.BoxNum;

                if (item.IsSingle == 1)
                {
                    btn.Label2.Text = "单警柜";
                }
                else
                {
                    btn.Label2.Text = "公共柜";
                }
                var user=glist.Where(p => p.BoxNum == item.BoxNum).FirstOrDefault();
                if (user != null)
                {
                    btn.Label3.Content = user.UserName;
                }
                else
                {
                    btn.Label3.Content = "未绑定";
                }
                if (item.IsLock==true)
                {
                    btn.Label4.Content = "已锁定";
                    btn.Label4.Foreground= (Brush)new BrushConverter().ConvertFromString("#ff0000");
                }
                else
                {
                    btn.Label4.Content = "未锁定";
                }
                btn.MouseDown += MouseButtonEventHandler;
                WrapPanel1.Children.Add(btn);
            }
        }
        public void MouseButtonEventHandler(object sender, MouseButtonEventArgs e)
        {
            var u = (BoxControl)sender;
            if (u.IsCheck)
            {
                u.IsCheck = false;

                var filepath = "pack://application:,,,/Image/a未选中.png";
                
                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                u.Label2.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                u.Label4.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
            }
            else
            {
                u.IsCheck = true;

                var filepath = "pack://application:,,,/Image/a选中.png";

                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                u.Label2.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                u.Label4.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
            }
        }
        //开箱
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var list = GetSelectBox();
            if (list.Count > 0)
            {
                string str = string.Join(",", list.ToArray());
                Task.Run(() =>
                {
                    foreach (var item in list)
                    {
                        LockManager.GetInstance().OpenBox(item);
                        Thread.Sleep(800);
                    }
                });
                var biz=new CommBiz();

                biz.EditAdminLogInfo(str+"开箱成功", Main.UserName);
                MessageDialog.ShowDialog("所选箱门已打开");
            }
        }
        //全开
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            var biz = new CommBiz();
            Task.Run(() =>
            {
                var list = biz.GetBoxList();
                foreach (var item in list)
                {
                    LockManager.GetInstance().OpenBox(item.BoxNum);
                    Thread.Sleep(1000);
                }
            });
            biz.EditAdminLogInfo("全开箱门操作成功", Main.UserName);
            MessageDialog.ShowDialog("所选箱门已打开");
        }
        //清箱
        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            var biz = new CommBiz();
            var list = GetSelectBox();
            if (list.Count > 0)
            {
                foreach (BoxControl item in WrapPanel1.Children)
                {
                    if (list.Contains(item.BoxNum))
                    {
                        item.Label2.Text = "空闲";
                    }
                }

                string str = string.Join(",", list.ToArray());
                biz.ClearBoxList(list);

                biz.EditAdminLogInfo(str + "清箱成功", Main.UserName);
                MessageDialog.ShowDialog(str + "清箱成功");
            }
        }
        //全清
        private void Button_Click3(object sender, RoutedEventArgs e)
        {
            foreach (BoxControl item in WrapPanel1.Children)
            {
                item.Label2.Text = "空闲";
            }

            var biz = new CommBiz();
            biz.AllClearBoxinfo();
            biz.EditAdminLogInfo("全部清箱成功", Main.UserName);
            MessageDialog.ShowDialog("全部清箱成功");
        }
        //锁箱
        private void Button_Click4(object sender, RoutedEventArgs e)
        {
            var biz = new CommBiz();
            var list = GetSelectBox();
            if (list.Count > 0)
            {
                string str = string.Join(",", list.ToArray());
                biz.LockBoxList(list);

                biz.EditAdminLogInfo(str + "锁箱成功", Main.UserName);
                MessageDialog.ShowDialog("所选箱门已锁");

                Init();
            }
        }
        //解锁
        private void Button_Click5(object sender, RoutedEventArgs e)
        {
            var biz = new CommBiz();
            var list = GetSelectBox();
            if (list.Count > 0)
            {
                string str = string.Join(",", list.ToArray());
                biz.UnLockBoxList(list);

                biz.EditAdminLogInfo(str + "解箱成功", Main.UserName);
                MessageDialog.ShowDialog("所选箱门已解锁");

                Init();
            }
        }
        private List<int> GetSelectBox()
        {
            List<int> list = new List<int>();
            foreach (BoxControl item in WrapPanel1.Children)
            {
                if (item.IsCheck)
                {
                    list.Add(item.BoxNum);
                }
            }
            return list;
        }
        private void Page_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Init();
        }
        private void SCManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
        //全选
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Button click = (Button)sender;
            var name = (string)click.Content;


            foreach (BoxControl item in WrapPanel1.Children)
            {
                var u = item;
                if (name != "全选")
                {
                    click.Content = "全选";

                    u.IsCheck = false;

                    var filepath = "pack://application:,,,/Image/a未选中.png";

                    u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                    u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                    u.Label2.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                    u.Label4.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                    u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                }
                else
                {
                    click.Content = "不选";

                    u.IsCheck = true;

                    var filepath = "pack://application:,,,/Image/a选中.png";

                    u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                    u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                    u.Label2.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                    u.Label4.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                    u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var list = GetSelectBox();
            if (list.Count > 0)
            {
                var type = Convert.ToInt32(((ComboBoxItem) ComboBox1.SelectedItem).Tag);
                var biz = new CommBiz();
                var result=biz.EditBoxType(list, type);
                MessageDialog.ShowDialog(result.message);
                Init();
            }
        }
    }
}
