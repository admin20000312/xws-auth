﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DJ.Dialog;
using NSJL.Biz.Background.Client;
using NSJL.Framework.Utils;
using NSJL.Plugin.Excel;

namespace DJ.Pages
{
    /// <summary>
    /// ManagerPage.xaml 的交互逻辑
    /// </summary>
    public partial class ManagerPage : Page
    {
        public ManagerPage()
        {
            InitializeComponent();

            var biz = new CommBiz();
            var list=biz.GetAdminLogList(null,null);

            DataGrid1.ItemsSource = list;
        }

        //显示
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var time = DatePicker1.Text;
            var start = time.ToDateTime().ToShortDateString().ToDateTime();
            var end = time.ToDateTime().ToString("yyyy-MM-dd 23:59:59").ToDateTime();

            var biz = new CommBiz();
            var list=biz.GetAdminLogList(start, end);
            DataGrid1.ItemsSource = list;
        }
        //显示全部
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            var biz = new CommBiz();
            var list = biz.GetAdminLogList(null, null);

            DataGrid1.ItemsSource = list;
        }
        //导出报表
        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            var path=SelfUtil.ChooseSaveFile();
            if (path == null)
            {
                return;
            }
            var biz = new CommBiz();
            var list = biz.GetAdminLogList(null, null);

            var table = SelfUtil.ListToDataTable(list);
            table.Columns.Remove(table.Columns["Id"]);
            table.Columns.Remove(table.Columns["UserName"]);

            table.Columns["Content"].ColumnName = "操作内容";
            table.Columns["CreateTime"].ColumnName = "操作时间";

            var excel = new ExcelHelper();
            excel.FillDataNew("sheet1", table, "", false);
            excel.SaveExcel(path);

            MessageDialog.ShowDialog("导出成功");

        }
    }
}
