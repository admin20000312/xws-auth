﻿using DJ.Dialog;
using NSJL.Biz.Background.Client;
using NSJL.Framework.Utils;
using NSJL.Plugin.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NSJL.DAL.DataModel.Entities;

namespace DJ.Pages
{
    /// <summary>
    /// AccessPage.xaml 的交互逻辑
    /// </summary>
    public partial class AccessPage : Page
    {
        public AccessPage()
        {
            InitializeComponent();
        }
        //查询
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var time = DatePicker1.Text;
            var endtime = DatePicker1_Copy.Text;
            //var type = (string)ComboBox1.SelectionBoxItem;
            //type = type == "操作" ? "" : type;
            DateTime? start = null;
            DateTime? end = null;
            if (!string.IsNullOrWhiteSpace(time))
            {
                start = time.ToDateTime().ToShortDateString().ToDateTime();
            }
            if (!string.IsNullOrWhiteSpace(endtime))
            {
                end = endtime.ToDateTime().ToString("yyyy-MM-dd 23:59:59").ToDateTime();
            }

            var biz = new CommBiz();
            var list = biz.GetOpenBoxList(null, start, end);
            DataGrid1.ItemsSource = list;
        }
        //显示全部
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            var biz = new CommBiz();
            var list = biz.GetOpenBoxList(null, null, null);
            DataGrid1.ItemsSource = list;
        }
        //导出报表
        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            var path = SelfUtil.ChooseSaveFile();
            if (path == null)
            {
                return;
            }
            var biz = new CommBiz();
            var list = biz.GetOpenBoxList(null, null,null);
            if (list.Count == 0)
            {
                return;
            }

            var table = SelfUtil.ListToDataTable(list);
            table.Columns.Remove(table.Columns["Id"]);

            table.Columns["UserName"].ColumnName = "用户名";
            table.Columns["BoxNum"].ColumnName = "箱门号";
            table.Columns["Type"].ColumnName = "类型";
            //table.Columns["UserCode"].ColumnName = "警号";
            table.Columns["GoodsName"].ColumnName = "物品名称";
            table.Columns["CreateTime"].ColumnName = "操作时间";
            table.Columns["LabelNum"].ColumnName = "标签号";
            table.Columns["Mode"].ColumnName = "认证方式";

            var excel = new ExcelHelper();
            excel.FillDataNew("sheet1", table, "", false);
            excel.SaveExcel(path);

            MessageDialog.ShowDialog("导出成功");
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            DatePicker1.Text = null;
            DatePicker1_Copy.Text = null;
        }

        private void Page_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var biz = new CommBiz();
            var list = biz.GetOpenBoxList(null, null, null);
            DataGrid1.ItemsSource = list;

            DatePicker1.Text = null;
            DatePicker1_Copy.Text = null;

        }
      
    }
}
