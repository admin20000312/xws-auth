﻿using NSJL.Biz.Background.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DJ.Clients;
using NSJL.DAL.DataModel.Entities;
using DJ.Dialog;
using NSJL.DomainModel.Background.Client;
using NSJL.Framework.Utils;
using NSJL.Plugin.Excel;

namespace DJ.Pages
{
    /// <summary>
    /// GoodsPage.xaml 的交互逻辑
    /// </summary>
    public partial class GoodsPage : Page
    {
        public GoodsPage()
        {
            InitializeComponent();
        }
        private void Page_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Init(null);
        }
        public void Init(string name)
        {
            var biz = new CommBiz();
            var list = biz.GetGoodsTypeList(name);
            DataGrid1.ItemsSource = null;
            DataGrid1.ItemsSource = list;
        }
        //查询
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var name = TextBox1.Text;
            Init(name);
        }
        //导出
        private void Button_Click3(object sender, RoutedEventArgs e)
        {
            var path = SelfUtil.ChooseSaveFile();
            if (path == null)
            {
                return;
            }
            var biz = new CommBiz();
            var list = biz.GetGoodsTypeList(null);
            if (list.Count == 0)
            {
                return;
            }

            var table = SelfUtil.ListToDataTable(list);
            //table.Columns.Remove(table.Columns["Id"]);
            table.Columns["Id"].ColumnName = "唯一编码";
            table.Columns["Name"].ColumnName = "物品名称";
            table.Columns["PartNum"].ColumnName = "物料编号";
            table.Columns["LabelNum"].ColumnName = "标签号";
            table.Columns["BoxNum"].ColumnName = "原箱门号";
            table.Columns["ReaBoxNum"].ColumnName = "实际箱门号";
            table.Columns["Type"].ColumnName = "状态";
            table.Columns["Remarks"].ColumnName = "备注";
            table.Columns["CreateTime"].ColumnName = "创建时间";

            var excel = new ExcelHelper();
            excel.FillDataNew("sheet1", table, "", false);
            excel.SaveExcel(path);

            MessageDialog.ShowDialog("导出成功");
        }

        //添加
        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            GoodsEdit user = new GoodsEdit();
            if (user.ShowDialog() != true)
            {
                return;
            }
            var name = user.Names.Text;
            var PartNum = user.PartNum.Text;
            var LabelNum = user.LabelNum.Text;
            var boxnum = user.ComboBox1.Text;
            var Remarks = user.Remarks.Text;
            if (string.IsNullOrWhiteSpace(boxnum))
            {
                MessageDialog.ShowDialog("请选择箱门");
                return;
            }
            var boxnums = boxnum.ToInt32();

            var biz=new CommBiz();
            var call = biz.EditGoodsInfo(null, name, PartNum, LabelNum, boxnums, Remarks);
            if (!call.result)
            {
                MessageDialog.ShowDialog(call.message);
                return;
            }
            MessageDialog.ShowDialog("添加成功");
            Init(null);

        }
        //编辑
        private void Button_Click4(object sender, RoutedEventArgs e)
        {
            GoodsInfo info = (GoodsInfo)DataGrid1.SelectedItem;
            if (info == null)
            {
                MessageDialog.ShowDialog("未选中用户行");
                return;
            }
            GoodsEdit user = new GoodsEdit();
            //赋值
            user.Names.Text = info.Name;
            user.PartNum.Text = info.PartNum;
            user.LabelNum.Text = info.LabelNum;
            user.Remarks.Text = info.Remarks;
            if (info.BoxNum != null)
            {
                var list = user.ComboBox1.Items;
                foreach (ComboBoxItem item in list)
                {
                    var temp = (int)item.Content;
                    if (info.BoxNum == temp)
                    {
                        user.ComboBox1.SelectedItem = item;
                    }
                }
            }
            if (user.ShowDialog() != true)
            {
                return;
            }
            var name = user.Names.Text;
            var PartNum = user.PartNum.Text;
            var LabelNum = user.LabelNum.Text;
            var boxnum = user.ComboBox1.Text;
            var Remarks = user.Remarks.Text;
            if (string.IsNullOrWhiteSpace(boxnum))
            {
                MessageDialog.ShowDialog("请选择箱门");
                return;
            }
            var boxnums = boxnum.ToInt32();

            var biz = new CommBiz();
            var call = biz.EditGoodsInfo(info.Id, name, PartNum, LabelNum, boxnums, Remarks);
            if (!call.result)
            {
                MessageDialog.ShowDialog(call.message);
                return;
            }
            MessageDialog.ShowDialog("编辑成功");
            Init(null);
        }
        //删除
        private void Button_Click5(object sender, RoutedEventArgs e)
        {
            GoodsInfo info = (GoodsInfo)DataGrid1.SelectedItem;
            if (info == null)
            {
                MessageDialog.ShowDialog("未选中用户行");
                return;
            }

            var biz=new CommBiz();
            biz.DelGoodsInfo(info.Id);
            Init(null);
            MessageDialog.ShowDialog("删除成功");
        }
        //导入
        private void Button_Click103(object sender, RoutedEventArgs e)
        {
            try
            {
                string filePath = SelfUtil.ChooseFile();
                if (string.IsNullOrWhiteSpace(filePath))
                {
                    return;
                }
                ExcelHelper excel = new ExcelHelper(filePath);
                var dt = excel.GetDataTable("sheet1", true);

                var biz = new CommBiz();
                biz.ImportExcelWithGoods2(dt);

                MessageDialog.ShowDialog("导入成功");
                Init(null);
            }
            catch (Exception exception)
            {
                TextLogUtil.Info(exception.Message);
                MessageDialog.ShowDialog("导入失败，请检查数据格式");
            }
        }

        private void TextBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }
    }
}
