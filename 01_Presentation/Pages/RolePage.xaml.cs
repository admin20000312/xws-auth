﻿using DJ.Dialog;
using NSJL.Biz.Background.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NSJL.DAL.DataModel.Entities;

namespace DJ.Pages
{
    /// <summary>
    /// RolePage.xaml 的交互逻辑
    /// </summary>
    public partial class RolePage : Page
    {
        public RolePage()
        {
            InitializeComponent();
        }
        private void Page_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Init();
        }
        public void Init()
        {
            //var biz = new CommBiz();
            //var list = biz.GetRoleList();

            //DataGrid1.ItemsSource = null;
            //DataGrid1.ItemsSource = list;
        }
        //添加
        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            //RoleEdit info=new RoleEdit();
            //if (info.ShowDialog() != true)
            //{
            //    return;
            //}
            
            //var type = ((ComboBoxItem) info.ComboBox1.SelectedItem).Content.ToString();
            //int? small = null;
            //int? big = null;
            //if (((ComboBoxItem) info.ComboBox2.SelectedItem) != null)
            //{
            //    small = Convert.ToInt32(((ComboBoxItem) info.ComboBox2.SelectedItem).Content);
            //}
            //if (((ComboBoxItem)info.ComboBox3.SelectedItem) != null)
            //{
            //    big = Convert.ToInt32(((ComboBoxItem)info.ComboBox3.SelectedItem).Content);
            //}
            //var biz = new CommBiz();
            //var result=biz.EditRoleInfo(null, info.name.Text, type, small,big);

            //MessageDialog.ShowDialog("操作成功");
            //Init();
        }
        //编辑
        private void Button_Click4(object sender, RoutedEventArgs e)
        {
            //RoleInfo data = (RoleInfo)DataGrid1.SelectedItem;
            //if (data == null)
            //{
            //    MessageDialog.ShowDialog("未选中行");
            //    return;
            //}
            //RoleEdit info = new RoleEdit(data.SmallBox,data.BigBox);
            //if (data.Type == "支队")
            //{
            //    info.ComboBox1.SelectedIndex = 1;
            //}
            //info.name.Text = data.Name;
            //if (info.ShowDialog() != true)
            //{
            //    return;
            //}

            //var type = ((ComboBoxItem)info.ComboBox1.SelectedItem).Content.ToString();
            //int? small = null;
            //int? big = null;
            //if (((ComboBoxItem)info.ComboBox2.SelectedItem) != null)
            //{
            //    small = Convert.ToInt32(((ComboBoxItem)info.ComboBox2.SelectedItem).Content);
            //}
            //if (((ComboBoxItem)info.ComboBox3.SelectedItem) != null)
            //{
            //    big = Convert.ToInt32(((ComboBoxItem)info.ComboBox3.SelectedItem).Content);
            //}
            //var biz = new CommBiz();
            //var result = biz.EditRoleInfo(data.Id, info.name.Text, type, small, big);

            //MessageDialog.ShowDialog("操作成功");
            //Init();
        }
        //删除
        private void Button_Click5(object sender, RoutedEventArgs e)
        {
            //RoleInfo data = (RoleInfo)DataGrid1.SelectedItem;
            //if (data == null)
            //{
            //    MessageDialog.ShowDialog("未选中行");
            //    return;
            //}
            //var biz = new CommBiz();
            //biz.DelRoleInfo(data.Id);
            //MessageDialog.ShowDialog("操作成功");
            //Init();
        }
        //解绑
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //RoleInfo data = (RoleInfo)DataGrid1.SelectedItem;
            //if (data == null)
            //{
            //    MessageDialog.ShowDialog("未选中行");
            //    return;
            //}
            //var biz=new CommBiz();
            //biz.UnBindRoleInfo(data.Id);
            //MessageDialog.ShowDialog("操作成功");
            //Init();
        }
    }
}
