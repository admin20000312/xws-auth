﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DJ.Dialog
{
    /// <summary>
    /// MessageDialog.xaml 的交互逻辑
    /// </summary>
    public partial class MessageDialog : Window
    {
        public static void ShowDialog(string message)
        {
            var mes=new MessageDialog();
            mes.TextBlock1.Text = message;
            mes.ShowDialog();
        }
        DispatcherTimer timer = new DispatcherTimer();
        public MessageDialog()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent(); 
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += ((a, b) =>
            {
                var miao = Label1.Tag.ToInt32() - 1;
                Label1.Tag = miao;
                if (miao <= 0)
                {
                    this.Close();
                    return;
                }
                Label1.Content = "倒计时：" + Label1.Tag + "秒";
            });
            timer.IsEnabled = true;
            timer.Start();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.DragMove();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            timer.IsEnabled = false;
            timer.Stop();
        }

        
    }
}
