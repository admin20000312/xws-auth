﻿using NSJL.DAL.DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DJ.Clients;
using NSJL.Biz.Background.Client;
using NSJL.Framework.Utils;
using NSJL.Plugin.Third.ThirdParty;

namespace DJ.Dialog
{
    /// <summary>
    /// GoodsEdit.xaml 的交互逻辑
    /// </summary>
    public partial class GoodsEdit : Window
    {
        public GoodsEdit()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();


            var biz = new CommBiz();
            var list = biz.GetBoxList();
            foreach (var item in list)
            {
                ComboBoxItem box = new ComboBoxItem();
                box.Content = item.BoxNum;
                ComboBox1.Items.Add(box);
            }
            if (ComboBox1.Items.Count > 0)
            {
                ComboBox1.SelectedIndex = 0;
            }



            Task.Run(() =>
            {
                try
                {
                    var ini = new IniFile("Config/Config.ini");
                    var isOpen = ini.readKey("LabelConfig", "isOpen");
                    if (isOpen == "1")
                    {
                        var portName = ini.readKey("LabelConfig", "portName");
                        var baudRate = ini.readKey("LabelConfig", "baudRate").ToInt32();
                        RFIDHelper9.GetInstance().ConnectCOM(portName, baudRate);
                        RFIDHelper9.GetInstance().CallBack = (a, b) => {
                            Dispatcher.Invoke(() =>
                            {
                                LabelNum.Text = a;
                            });
                        };
                        RFIDHelper9.GetInstance().StartRead();
                    }
                }
                catch (Exception e)
                {

                }
            });

        }
        public string Temp = null;
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Capital)
            {
                Temp = "";
            }
            else if (Temp != null && e.Key != Key.Return)
            {
                if (e.Key.ToString().StartsWith("D") && e.Key.ToString().Length == 2)
                {
                    var temp = e.Key.ToString().Replace("D", "");
                    Temp += temp;
                }
                else
                {
                    var temp = e.Key.ToString();
                    Temp += temp;
                }
            }
            else if (Temp != null && e.Key == Key.Return)
            {
                LabelNum.Text = Temp;
                Temp = null;
            }
        }
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Names.Text))
            {
                MessageDialog.ShowDialog("名称不能为空");
                return;
            }
            if (string.IsNullOrWhiteSpace(LabelNum.Text))
            {
                MessageDialog.ShowDialog("标签号不能为空");
                return;
            }
            
            this.DialogResult = true;
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.DragMove();
        }

        private void Names_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            var ini = new IniFile("Config/Config.ini");
            var isOpen = ini.readKey("LabelConfig", "isOpen");
            if (isOpen == "1")
            {
                try
                {
                    RFIDHelper9.GetInstance().StopRead();
                    RFIDHelper9.GetInstance().Close();
                }
                catch (Exception exception)
                {
                }
            }
        }
    }
}
