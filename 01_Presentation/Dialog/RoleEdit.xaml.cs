﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NSJL.Biz.Background.Client;
using NSJL.Framework.Utils;

namespace DJ.Dialog
{
    /// <summary>
    /// RoleEdit.xaml 的交互逻辑
    /// </summary>
    public partial class RoleEdit : Window
    {
        public RoleEdit(int? small=null,int? big=null)
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();


            //var biz = new CommBiz();
            //var list=biz.GetBoxList();
            //var rlist=biz.GetRoleList();
            //foreach (var item in list)
            //{
            //    if (item.BoxType == "小")
            //    {
            //        if (!rlist.Any(p => p.SmallBox == item.BoxNum))
            //        {
            //            ComboBoxItem box = new ComboBoxItem();
            //            box.Content = item.BoxNum.ToString();
            //            ComboBox2.Items.Add(box);
            //        }
            //    }else if (item.BoxType == "大")
            //    {
            //        if (!rlist.Any(p => p.BigBox == item.BoxNum))
            //        {
            //            ComboBoxItem box = new ComboBoxItem();
            //            box.Content = item.BoxNum.ToString();
            //            ComboBox3.Items.Add(box);
            //        }
            //    }
            //}

            //if (small != null)
            //{
            //    ComboBoxItem box = new ComboBoxItem();
            //    box.Content = small.ToString();
            //    ComboBox2.Items.Add(box);
            //    ComboBox2.SelectedItem = box;
            //}
            //else
            //{
            //    if (ComboBox2.Items.Count > 0)
            //    {
            //        ComboBox2.SelectedIndex = 0;
            //    }
            //}

            //if (big != null)
            //{
            //    ComboBoxItem box = new ComboBoxItem();
            //    box.Content = big.ToString();
            //    ComboBox3.Items.Add(box);
            //    ComboBox3.SelectedItem = box;
            //}
            //else
            //{
            //    if (ComboBox3.Items.Count > 0)
            //    {
            //        ComboBox3.SelectedIndex = 0;
            //    }
            //}

        }
        //当改变选中项的时候
        private void ComboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                ComboBoxItem item= (ComboBoxItem)e.AddedItems[0];
                if ((string)item.Content == "大队")
                {
                    ComboBox2.Visibility = Visibility.Visible;
                    ComboBox3.Visibility = Visibility.Visible;
                }else if ((string)item.Content == "支队")
                {
                    ComboBox2.Visibility = Visibility.Hidden;
                    ComboBox3.Visibility = Visibility.Hidden;
                }
            }
        }

        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(name.Text))
            {
                MessageDialog.ShowDialog("名称不能为空");
                return;
            }
            DialogResult = true;
            Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.DragMove();
        }

        private void name_GotFocus(object sender, RoutedEventArgs e)
        {
            SelfUtil.Open();
        }
    }
}
