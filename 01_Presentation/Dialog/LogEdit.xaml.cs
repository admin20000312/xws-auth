﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DJ.Clients;
using NSJL.Biz.Background.Client;
using NSJL.Framework.Utils;
using NSJL.Plugin.Third.ThirdParty;

namespace DJ.Dialog
{
    /// <summary>
    /// LogEdit.xaml 的交互逻辑
    /// </summary>
    public partial class LogEdit : Window
    {
        public LogEdit()
        {
            InitializeComponent();
            ComWindow.Start(this);

            Init(null, null, null);



            Task.Run(() =>
            {
                try
                {
                    var ini = new IniFile("Config/Config.ini");
                    var isOpen = ini.readKey("LabelConfig", "isOpen");
                    if (isOpen == "1")
                    {
                        var portName = ini.readKey("LabelConfig", "portName");
                        var baudRate = ini.readKey("LabelConfig", "baudRate").ToInt32();
                        RFIDHelper9.GetInstance().ConnectCOM(portName, baudRate);

                        RFIDHelper9.GetInstance().CallBack = (a, b) => {
                            Dispatcher.Invoke(() =>
                            {
                                LabelNum.Text = a;
                            });
                        };
                        RFIDHelper9.GetInstance().StartRead();
                    }
                }
                catch (Exception e)
                {

                }
            });
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.DragMove();
        }

        private void TextBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }

        private void Button_Click1111(object sender, RoutedEventArgs e)
        {
            Init(names.Text,PartNum.Text,LabelNum.Text);
        }
        public void Init(string name,string partNum,string labelNum)
        {
            var biz = new CommBiz();
            var list = biz.GetGoodsTypeList(name, partNum, labelNum);
            DataGrid1.ItemsSource = null;
            DataGrid1.ItemsSource = list;
        }

        public string Temp = null;
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Capital)
            {
                Temp = "";
            }
            else if (Temp != null && e.Key != Key.Return)
            {
                if (e.Key.ToString().StartsWith("D") && e.Key.ToString().Length == 2)
                {
                    var temp = e.Key.ToString().Replace("D", "");
                    Temp += temp;
                }
                else
                {
                    var temp = e.Key.ToString();
                    Temp += temp;
                }
            }
            else if (Temp != null && e.Key == Key.Return)
            {
                LabelNum.Text = Temp;
                Temp = null;
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            var ini = new IniFile("Config/Config.ini");
            var isOpen = ini.readKey("LabelConfig", "isOpen");
            if (isOpen == "1")
            {
                try
                {
                    RFIDHelper9.GetInstance().StopRead();
                    RFIDHelper9.GetInstance().Close();
                }
                catch (Exception exception)
                {
                }
            }
        }
    }
}
