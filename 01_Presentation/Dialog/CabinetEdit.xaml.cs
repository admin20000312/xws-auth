﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NSJL.Biz.Background.Client;
using NSJL.Framework.Utils;

namespace DJ.Dialog
{
    /// <summary>
    /// CabinetEdit.xaml 的交互逻辑
    /// </summary>
    public partial class CabinetEdit : Window
    {
        public CabinetEdit()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
        }

        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            var biz = new CommBiz();
            var result=biz.EditCabinet(name.Text);
            if (result.result)
            {
                this.DialogResult = true;
                this.Close();
            }
            else
            {
                MessageDialog.ShowDialog(result.message);
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void name_GotFocus(object sender, RoutedEventArgs e)
        {
            SelfUtil.Open();
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.DragMove();
        }
    }
}
