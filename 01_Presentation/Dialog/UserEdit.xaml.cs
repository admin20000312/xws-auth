﻿using DJ.ZEF.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DJ.Clients;
using DJ.Third;
using NSJL.Biz.Background.Client;
using NSJL.Framework.Utils;
using System.IO;

namespace DJ.Dialog
{
    /// <summary>
    /// UserEdit.xaml 的交互逻辑
    /// </summary>
    public partial class UserEdit : Window
    {
        public UserEdit()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();

            var biz = new CommBiz();
            var list = biz.GetSinglePoliceList();
            foreach (var item in list)
            {
                ComboBoxItem box = new ComboBoxItem();
                box.Content = item.BoxNum;
                ComboBox1_Copy.Items.Add(box);
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        public string faceId { get; set; }
        public string faceBase64 { get; set; }
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(UserName.Text))
                {
                    MessageDialog.ShowDialog("用户名不能为空");
                    return;
                }

                var biz = new CommBiz();
                if (faceBytes != null && faceBytes.Length > 0)
                {
                    //删除原先的face
                    var result = BaiduFaceApiHelper.GetInstance().Contrast(faceBytes);
                    if (result.result)
                    {
                        if (faceId == result.data)
                        {
                            BaiduFaceApiHelper.GetInstance().UpdateFace(faceId, faceBytes);
                            faceBase64 = Convert.ToBase64String(faceBytes);
                        }
                        else
                        {
                            //去数据库查询人脸ID是否存在 如果存在不让添加  不存在

                            var isf = biz.IsFace(result.data);
                            if (isf)
                            {
                                MessageDialog.ShowDialog("人脸已存在，无法继续添加");
                                return;
                            }
                            else
                            {
                                BaiduFaceApiHelper.GetInstance().DeleteFace(result.data);
                                var res = BaiduFaceApiHelper.GetInstance().EditUserWithDB(faceBytes, faceId);
                                if (res.result)
                                {
                                    faceId = res.data;
                                    faceBase64 = Convert.ToBase64String(faceBytes);
                                }
                            }
                        }
                    }
                    else
                    {
                        var res = BaiduFaceApiHelper.GetInstance().EditUserWithDB(faceBytes, faceId);
                        if (res.result)
                        {
                            faceId = res.data;
                            faceBase64 = Convert.ToBase64String(faceBytes);
                        }
                    }
                }

                this.DialogResult = true;
            }
            catch (Exception exception)
            {
                TextLogUtil.Info(exception.Message);
                MessageDialog.ShowDialog("系统异常");
                return;
            }
           
        }
        //扫描卡号
        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            try
            {
                CardNo no = new CardNo();
                var result=no.ShowDialog();
                code.Text = no.TextBox1.Text;
            }
            catch (Exception ex)
            {
                TextLogUtil.Info(ex.Message);
                MessageDialog.ShowDialog(ex.Message);
            }
        }
      
        public byte[] faceBytes { get; set; }
        //识别人脸
        private void Button_Click5(object sender, RoutedEventArgs e)
        {
            try
            {
                Face fa = new Face();
                var result = fa.ShowDialog();
                if (result!=null && result.Value)
                {
                    faceBytes = (byte[])fa.Facebytes.Clone();
                    headpic.Source = (BitmapSource)new ImageSourceConverter().ConvertFrom(faceBytes);
                }
            }
            catch (Exception exception)
            {
                TextLogUtil.Info(exception.Message);
                MessageDialog.ShowDialog(exception.Message);
            }
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.DragMove();
        }

        private void UserName_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }

        private void Button_Click5111(object sender, RoutedEventArgs e)
        {
            //百度人脸照片  判断是否是人脸的图片
            System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
            //dialog.Multiselect = false;//该值确定是否可以选择多个文件
            dialog.Title = "请选择图片文件";
            dialog.Filter = "所有文件(*.*)|*.*";
            if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            {
                MessageDialog.ShowDialog("请选择文件");
                return;
            }

            var ex = System.IO.Path.GetExtension(dialog.FileName).ToLower();
            if (ex == ".jpg" || ex == ".jpeg" || ex == ".png" || ex == ".bmp")
            {
                FileInfo finfo = new FileInfo(dialog.FileName);
                if (finfo.Length > 512000)
                {
                    MessageDialog.ShowDialog("导入的文件不能大于500KB");
                    return;
                }


                var bitmapBytes = File.ReadAllBytes(dialog.FileName);

                var result = BaiduFaceApiHelper.GetInstance().Verification(bitmapBytes);
                if (result.result)
                {
                    faceBytes = (byte[])bitmapBytes.Clone();
                    headpic.Source = (BitmapSource)new ImageSourceConverter().ConvertFrom(faceBytes);

                }
                else
                {
                    MessageDialog.ShowDialog("请导入人脸图片");
                }
            }
            else
            {
                MessageDialog.ShowDialog("请选择图片文件");
                return;
            }
        }
    }
}
