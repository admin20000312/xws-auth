﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }





        //取CPU编号
        public String GetCpuID()
        {
            try
            {
                ManagementClass mc = new ManagementClass("Win32_Processor");
                ManagementObjectCollection moc = mc.GetInstances();
                String strCpuID = null;
                foreach (ManagementObject mo in moc)
                {
                    strCpuID = mo.Properties["ProcessorId"].Value.ToString();
                    break;
                }
                return strCpuID;
            }
            catch
            {
                return null;
            }
        }
        //取第⼀块硬盘编号
        public String GetHardDiskID()
        {
            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMedia");
                String strHardDiskID = null;
                foreach (ManagementObject mo in searcher.Get())
                {
                    strHardDiskID = mo["SerialNumber"].ToString().Trim();
                    break;
                }
                return strHardDiskID;
            }
            catch
            {
                return null;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var saddsads = GetCpuID();
            var sasda = GetHardDiskID();
            if (saddsads != null)
            {
                mama.Text = saddsads;
            }
            else
            {
                mama.Text = sasda;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(mama.Text))
            {
                System.Windows.MessageBox.Show("请输入机器码");
                return;
            }
            System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();

            dialog.FileName = string.Format("authorization.dll");
            dialog.Title = "请选择文件夹";
            //dialog.Filter = "Execl files (*.xls)|*.xls";
            if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            var FileName = dialog.FileName;
            var daysss = Convert.ToInt32(mama_Copy.Text.Trim());

            var Minute = Convert.ToInt32(day.Text.Trim()) * 24 * 60;
            var AuthCode = mama.Text;

            var ccc = Minute + "#" + DateTime.Now.AddDays(daysss).ToString("yyyy-MM-dd HH:mm:ss") + "#" + AuthCode;
            var base64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(ccc));
            File.WriteAllText(FileName, base64);

            System.Windows.MessageBox.Show("生成成功");
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                //dialog.Multiselect = true;      //该值确定是否可以选择多个文件
                dialog.Title = "请选择文件";     //弹窗的标题
                //dialog.InitialDirectory = "D:\\";       //默认打开的文件夹的位置
                //dialog.Filter = "MicroSoft Excel文件(*.xlsx)|*.xlsx|所有文件(*.*)|*.*";       //筛选文件
                //dialog.ShowHelp = true;     //是否显示“帮助”按钮

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    var content=File.ReadAllText( dialog.FileName);
                    var temp = Convert.FromBase64String(content);
                    var temp222 = Encoding.UTF8.GetString(temp);

                    var arr = temp222.Split('#');
                    //var num = arr[0].ToInt32();
                    //var time = arr[1].ToDateTime();
                    var authss = arr[2].Trim();

                    mama.Text = authss;
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }
    }
}
