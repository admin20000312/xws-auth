﻿using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataAccess
{
    public static class DBConnection
    {
        public static string GetConnectionString()
        {
            var ini=new IniFile("Config/Config.ini");
            var server= ini.readKey("Database", "server");
            var port = ini.readKey("Database", "port");
            var database = ini.readKey("Database", "database");
            var user = ini.readKey("Database", "user");
            var password = ini.readKey("Database", "password");

            //var str = "server="+ server + ";port="+ port + "; database="+ database + "; user="+ user + "; password="+ password + ";Character Set=utf8;";


            var str = "Data Source="+ server + ";port="+ port + ";Initial Catalog="+ database + ";user id="+ user + ";password="+ password + ";Character Set=utf8;SslMode=None;";  //

            //var str= ConfigurationManager.ConnectionStrings["SJLDbContext"].ConnectionString;
            //var aaa= AESHelper.AESDecrypt(str);

            //TextLogUtil.Info(str);
            return str;
        }

    }
}
