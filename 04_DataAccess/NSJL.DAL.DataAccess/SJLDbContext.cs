﻿using MySql.Data.Entity;
using NSJL.DAL.DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataAccess
{
    public class SJLDbContext : DbContext
    {
        public SJLDbContext()
            : base(DBConnection.GetConnectionString())
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();//移除复数表名的契约
            Database.SetInitializer<SJLDbContext>(null);

            //modelBuilder.Conventions.Remove<IncludeMetadataConvention>();//移除对MetaData表的查询验证，要不然每次都要访问EdmMe
            //tadata这个表
        }
        public DbSet<BoxInfo> BoxInfo { get; set; }
        public DbSet<UserInfo> UserInfo { get; set; }
        public DbSet<OpenBoxInfo> OpenBoxInfo { get; set; }
        public DbSet<AdminInfo> AdminInfo { get; set; }
        public DbSet<AdminLogInfo> AdminLogInfo { get; set; }

        public DbSet<GoodsInfo> GoodsInfo { get; set; }
        public DbSet<CabinetInfo> CabinetInfo { get; set; }


    }
}
