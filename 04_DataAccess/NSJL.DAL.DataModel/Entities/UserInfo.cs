﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataModel.Entities
{
    public class UserInfo
    {
        [Key]
        public string Id { get; set; }
        //头像 base64
        public string HeadPic { get; set; }
        //SDK里面的ID
        public string FaceId { get; set; }
        //创建时间
        public DateTime? CreateTime { get; set; }
        //卡号
        public string CardNum { get; set; }
        //绑定的箱门号
        public int? BoxNum { get; set; }
        //是否有公共箱门权限
        public int? IsPublicBox { get; set; }
        public string UserName { get; set; }
    }
}
