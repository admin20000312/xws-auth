﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataModel.Entities
{
    public class CabinetInfo
    {
        [Key]
        public string Id { get; set; }
        //姓名
        public string Name { get; set; }
        public DateTime? CreateTime { get; set; }
    }
}
