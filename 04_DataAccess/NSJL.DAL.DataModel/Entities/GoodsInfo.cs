﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataModel.Entities
{
    public class GoodsInfo
    {
        [Key]
        public string Id { get; set; }
        //名称
        public string Name { get; set; }
        //物料编号
        public string PartNum { get; set; }
        //标签号
        public string LabelNum { get; set; }
        //绑定的箱门号
        public int? BoxNum { get; set; }
        //实际存在的箱门号
        public int? ReaBoxNum { get; set; }
        //状态   在库  离库
        public string Type { get; set; }
        //备注
        public string Remarks { get; set; }
        public DateTime? CreateTime { get; set; }
    }
}
