﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataModel.Entities
{
    public class OpenBoxInfo
    {
        [Key]
        public string Id { get; set; }
        //操作类型   存   取
        public string Type { get; set; }
        //认证方式   人脸刷卡指纹
        public string Mode { get; set; }
        //箱门编号
        public int BoxNum { get; set; }
        //创建时间
        public DateTime? CreateTime { get; set; }
        //姓名
        public string UserName { get; set; }
        //物品名称
        public string GoodsName { get; set; }
        //标签号
        public string LabelNum { get; set; }

    }
}
