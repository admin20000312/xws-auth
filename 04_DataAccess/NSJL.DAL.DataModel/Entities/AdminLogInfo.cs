﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataModel.Entities
{
    public class AdminLogInfo
    {
        [Key]
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Content { get; set; }
        //创建时间
        public DateTime? CreateTime { get; set; }
        public string CabName { get; set; }
    }
}
