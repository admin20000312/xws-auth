﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.Framework.Utils
{
    public class CachedBuffer
    {
        private static int defaultBufferSize = 4096000;
        private int bufferSize;
        public int HeadQueue { get; set; }
        public byte[] DataBuff { get; set; }
        public int TailQueue { get; set; }
        public CachedBuffer()
        {
            bufferSize = defaultBufferSize;
            DataBuff = new byte[bufferSize];
            HeadQueue = 0;
            TailQueue = 0;
        }
        public CachedBuffer(int size)
        {
            if (size > 0)
            {
                bufferSize = size;
            }
            else
            {
                bufferSize = defaultBufferSize;
            }
            DataBuff = new byte[bufferSize];
            HeadQueue = 0;
            TailQueue = 0;
        }
        public void Clear()
        {
            HeadQueue = 0;
            TailQueue = 0;
            DataBuff = new byte[bufferSize];
        }
        //取走所有数据
        public byte[] Dequeue()
        {
            try
            {
                byte[] val = new byte[TailQueue - HeadQueue];
                for (int i = 0; i < val.Length; i++)
                {
                    val[i] = DataBuff[HeadQueue + i];
                }

                HeadQueue = 0;
                TailQueue = 0;
                return val;
            }
            catch (Exception ex)
            {
                TextLogUtil.Info(ex.Message);
            }
            return null;
        }
        public bool Write(byte recv)
        {
            if (TailQueue + 1 > bufferSize)
            {
                TailQueue = 0;
                HeadQueue = 0;
            }
            DataBuff[TailQueue] = recv;
            TailQueue += 1;
            return true;
        }
        public bool Write(byte[] recv, int start, int len)
        {
            try
            {
                if (TailQueue + len > bufferSize)
                {
                    TailQueue = 0;
                    HeadQueue = 0;
                }
                int endPos = start + len;
                for (int i = start; i < endPos; i++)
                {
                    DataBuff[TailQueue] = recv[i];
                    TailQueue += 1;
                }
                return true;
            }
            catch (Exception ex)
            {
                TextLogUtil.Info(ex.Message);
            }
            return false;
        }
        public byte[] ReadDataWithPlatform()
        {
            try
            {
                int tempTailQueue = TailQueue;
                int len = 0;
                int start_pos = -1;
                int end_pos = -1;
                if (tempTailQueue > HeadQueue)
                {
                    //开始符号
                    for (int i = HeadQueue; i < tempTailQueue; i++)
                    {
                        if (DataBuff[i] == 0x55)
                        {
                            len = 6;
                            start_pos = i;
                            end_pos = start_pos + len - 1;
                            break;
                        }
                    }
                    if (tempTailQueue < end_pos)
                    {
                        return null;
                    }
                    //判断数据是否有效
                    if (start_pos + 1 < end_pos)
                    {
                        if (start_pos > -1 && end_pos > -1 && end_pos - start_pos - 1 > 0)
                        {
                            byte[] recv = new byte[len];
                            int index = 0;
                            for (int i = start_pos; i <= end_pos; i++)
                            {
                                recv[index] = DataBuff[i];
                                index++;
                            }
                            HeadQueue = end_pos + 1;

                            ////校验开始----------
                            //                     var temp = new byte[len - 1];
                            //Array.Copy(recv,temp,temp.Length);
                            //                     var cs = SelfUtil.XorCheck(temp);
                            ////校验结束----------
                            //                     if (DataBuff[end_pos] != cs)
                            //                     {	//crc数据校验不通过   返回空
                            //                         TextLogUtil.Info("crc数据校验不通过   返回空");
                            //                         return null;
                            //                     }

                            return recv;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TextLogUtil.Info(ex.Message);
            }
            return null;
        }
        public byte[] ReadDataWithCard(int cardType)
        {
            try
            {
                int tempTailQueue = TailQueue;
                int len = 0;
                int start_pos = -1;
                int end_pos = -1;
                if (tempTailQueue > HeadQueue)
                {
                    //开始符号
                    for (int i = HeadQueue; i < tempTailQueue; i++)
                    {
                        if (cardType == 0)
                        {
                            if (DataBuff[i] == 0x04 && DataBuff[i + 1] == 0x0c && DataBuff[i + 2] == 0x02 && DataBuff[i + 3] == 0x20) // 0x00 表示成功
                            {
                                len = 12;
                                start_pos = i;
                                end_pos = start_pos + len - 1;
                                break;
                            }
                        }
                        else if (cardType == 1)
                        {
                            if (DataBuff[i] == 0x02)
                            {
                                len = 14;
                                start_pos = i;
                                end_pos = start_pos + len - 1;
                                break;
                            }
                        }
                        else if (cardType == 2)
                        {
                            if (DataBuff[i] == 0x02)
                            {
                                len = 12;
                                start_pos = i;
                                end_pos = start_pos + len - 1;
                                break;
                            }
                        }
                    }
                    if (tempTailQueue < end_pos)
                    {
                        return null;
                    }
                    //判断数据是否有效
                    if (start_pos + 1 < end_pos)
                    {
                        if (start_pos > -1 && end_pos > -1 && end_pos - start_pos - 1 > 0)
                        {
                            byte[] recv = new byte[len];
                            int index = 0;
                            for (int i = start_pos; i <= end_pos; i++)
                            {
                                recv[index] = DataBuff[i];
                                index++;
                            }
                            HeadQueue = end_pos + 1;

                            ////校验开始----------
                            //                     var temp = new byte[len - 1];
                            //Array.Copy(recv,temp,temp.Length);
                            //                     var cs = SelfUtil.XorCheck(temp);
                            ////校验结束----------
                            //                     if (DataBuff[end_pos] != cs)
                            //                     {	//crc数据校验不通过   返回空
                            //                         TextLogUtil.Info("crc数据校验不通过   返回空");
                            //                         return null;
                            //                     }

                            return recv;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TextLogUtil.Info(ex.Message);
            }
            return null;
        }
        public byte[] ReadDataWithPlatformNew()
        {
            try
            {
                int tempTailQueue = TailQueue;
                int len = 0;
                int start_pos = -1;
                int end_pos = -1;
                if (tempTailQueue > HeadQueue)
                {
                    //开始符号
                    for (int i = HeadQueue; i < tempTailQueue; i++)
                    {
                        if (DataBuff[i] == 0x5c && (DataBuff[i + 1] == 0xb2 || DataBuff[i + 1] == 0xb3))
                        {
                            len = 10;
                            start_pos = i;
                            end_pos = start_pos + len - 1;
                            break;
                        }
                        if (DataBuff[i] == 0x5c && (DataBuff[i + 1] == 0xbc || DataBuff[i + 1] == 0xbd))
                        {
                            len = 6;
                            start_pos = i;
                            end_pos = start_pos + len - 1;
                            break;
                        }
                    }
                    if (tempTailQueue < end_pos)
                    {
                        return null;
                    }
                    //判断数据是否有效
                    if (start_pos + 1 < end_pos)
                    {
                        if (start_pos > -1 && end_pos > -1 && end_pos - start_pos - 1 > 0)
                        {
                            byte[] recv = new byte[len];
                            int index = 0;
                            for (int i = start_pos; i <= end_pos; i++)
                            {
                                recv[index] = DataBuff[i];
                                index++;
                            }
                            HeadQueue = end_pos + 1;

                            //var temp = new byte[len - 1];
                            //Array.Copy(recv, temp, temp.Length);
                            //var cs = SelfUtil.XorCheck(temp);
                            ////校验结束----------
                            //if (DataBuff[end_pos] != cs)
                            //{   //crc数据校验不通过   返回空
                            //    TextLogUtil.Info("crc数据校验不通过   返回空");
                            //    return null;
                            //}
                            return recv;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TextLogUtil.Info(ex.Message);
            }
            return null;
        }
    }
}
