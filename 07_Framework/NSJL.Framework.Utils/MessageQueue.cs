﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.Framework.Utils
{
    public class MessageQueue
    {
        int MAX_COUNTS = 300;
        Queue queueMessage;
        public Action<object> MessageEnqueued { get; set; }
        public MessageQueue()
        {
            queueMessage = Queue.Synchronized(new Queue(MAX_COUNTS));
        }
        public MessageQueue(int max_count)
        {
            MAX_COUNTS = max_count;
            queueMessage = Queue.Synchronized(new Queue(MAX_COUNTS));
        }
        public int GetMessageCount()
        {
            return queueMessage.Count;
        }
        public void SetMessageCapcity(int capcity)
        {
            MAX_COUNTS = capcity;
        }
        public void Enqueue(object mb)
        {
            try
            {
                if (MAX_COUNTS <= 0)
                    return;
                queueMessage.Enqueue(mb);
                if (queueMessage.Count > MAX_COUNTS)
                {
                    queueMessage.Dequeue();
                }
                if (MessageEnqueued != null)
                {
                    MessageEnqueued(mb);
                }
            }
            catch
            {
            }
        }
        public void Clear()
        {
            queueMessage.Clear();
        }
        /// <summary>
        /// 消息出队列
        /// </summary>
        public object Dequeue()
        {
            try
            {
                if (queueMessage.Count > 0)
                {
                    return (object)queueMessage.Dequeue();
                }
            }
            catch
            {
            }
            return null;
        }
        /// <summary>
        /// 取对头消息
        /// </summary>
        public object Peek()
        {
            try
            {
                if (queueMessage.Count > 0)
                {
                    return (object)queueMessage.Peek();
                }
            }
            catch
            {
            }
            return null;
        }

    }
}
